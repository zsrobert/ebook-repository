package ftn.udd.projekt.service.impl.db;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.model.User;
import ftn.udd.projekt.repository.UserRepository;
import ftn.udd.projekt.service.def.UserService;

@Service
public class DBUserService implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User findOne(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public User save(User item) {
		return userRepository.save(item);
	}

	@Override
	public void delete(Integer id) {
		userRepository.delete(id);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public List<User> findBySubscription(Category category) {
		return userRepository.findAllWithSubscription(category);
	}

}
