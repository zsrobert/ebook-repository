package ftn.udd.projekt.lucene.worker;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class IndexWorker {
	private BlockingQueue<Runnable> jobs = new ArrayBlockingQueue<>(100,
			true);
	private Thread worker;
	private boolean working = true;

	@PostConstruct
	public void init() {
		worker = new Thread(new Runnable() {
			@Override
			public void run() {
				while (working) {
					Runnable job = jobs.poll();

					if (job != null) {
						job.run();
					}
				}
			}
		});
		worker.start();
	}

	public void stop() {
		this.working = false;
	}
	
	public void addJob(Runnable job) {
		jobs.add(job);
	}
}
