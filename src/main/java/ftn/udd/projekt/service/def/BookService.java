package ftn.udd.projekt.service.def;

import java.util.List;

import ftn.udd.projekt.model.Book;

public interface BookService extends BaseService<Book> {

	public List<Book> findAllInCategory(Integer id);
	public List<Book> findAllByAuthor(Integer id);
}
