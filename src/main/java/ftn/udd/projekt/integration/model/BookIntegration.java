package ftn.udd.projekt.integration.model;


public class BookIntegration {
	private String id;
	
	private String title;
	
	public BookIntegration(String bookId) {
		this.id = bookId;
	}

	public BookIntegration() {
		// TODO Auto-generated constructor stub
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public BookIntegration(String bookId, String title) {
		super();
		this.id = bookId;
		this.title = title;
	}

	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookIntegration other = (BookIntegration) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
