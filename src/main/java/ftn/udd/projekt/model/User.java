package ftn.udd.projekt.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import ftn.udd.projekt.dto.UserDTO;

@Entity
@Table(name = "user_info")
public class User extends BaseModel implements Serializable {

	private static final long serialVersionUID = -7552277512465802313L;

	@Column(nullable = false, length = 255, unique = true)
	@Email
	private String email;

	@Column(nullable = false, length = 255)
	@NotBlank
	private String firstName;

	@Column
	private boolean isAdmin = false;

	@Column(nullable = false, length = 255)
	@NotBlank
	private String lastName;

	@Column(nullable = false, length = 255)
	@Length(min = 5)
	@NotBlank
	private String password;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(nullable = true, name = "subscription_id")
	private Subscription subscription;

	public User() {
		super();
	}

	public User(UserDTO userDTO) {
		this.firstName = userDTO.getFirstName();
		this.lastName = userDTO.getLastName();
		this.email = userDTO.getEmail();
		this.password = userDTO.getPassword();
		this.id = userDTO.getId();
		this.isAdmin = userDTO.isAdmin();

		if (userDTO.getSubscribedTo() == null
				|| userDTO.getSubscribedTo().getId() == -1) {
			this.subscription = null;
		}

		if (userDTO.getSubscribedTo() != null
				&& userDTO.getSubscribedTo().getId() == 0) {
			this.subscription = new Subscription();
			this.subscription.setUser(this);
		}

		if (userDTO.getSubscribedTo() != null
				&& userDTO.getSubscribedTo().getId() > 0) {
			this.subscription = new Subscription();
			this.subscription.setCategory(new Category(userDTO
					.getSubscribedTo()));
			this.subscription.setUser(this);
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", firstName=" + firstName
				+ ", isAdmin=" + isAdmin + ", lastName=" + lastName
				+ ", password=" + password + ", subscription="
				+ (subscription != null ? subscription.toString() : "none")
				+ "]";
	}

}
