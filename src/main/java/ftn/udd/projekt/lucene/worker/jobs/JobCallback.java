package ftn.udd.projekt.lucene.worker.jobs;

public interface JobCallback {
	public void success();
	public void failure();
}
