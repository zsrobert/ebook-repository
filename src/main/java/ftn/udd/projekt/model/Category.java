package ftn.udd.projekt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import ftn.udd.projekt.dto.CategoryDTO;

@Entity
@Table(name = "category_info")
public class Category extends BaseModel implements Serializable {
	private static final long serialVersionUID = -3705811841638320806L;

	@Column(nullable = false, length = 255)
	@NotBlank
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Category (CategoryDTO categoryDTO) {
		this.name = categoryDTO.getName();
		this.id = categoryDTO.getId();
	}

	public Category() {
		super();
	}
}
