package ftn.udd.projekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.udd.projekt.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
