package ftn.udd.projekt.service.impl.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.repository.AuthorRepository;
import ftn.udd.projekt.repository.BookRepository;
import ftn.udd.projekt.service.def.BookService;

@Service
public class DBBookService implements BookService {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private AuthorRepository authorRepository;

	@Override
	public Book findOne(Integer id) {
		return bookRepository.findOne(id);
	}

	@Override
	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	@Override
	@Transactional
	public Book save(Book item) {
		Set<Author> authors = new HashSet<>();

		if (item.getAuthors() != null) {
			for (Author author : item.getAuthors()) {
				if (author.getId() == null && author.getName() != null
						&& !author.getName().isEmpty()) {
					Author a = authorRepository.findByName(author.getName());

					if (a != null) {
						authors.add(a);
					} else {
						authors.add(authorRepository.saveAndFlush(author));
					}
				} else {
					authors.add(author);
				}
			}
			
			item.setAuthors(authors);
		}

		return bookRepository.save(item);
	}

	@Override
	public void delete(Integer id) {
		bookRepository.delete(id);
	}

	@Override
	public List<Book> findAllInCategory(Integer id) {
		return bookRepository.findAllInCategory(id);
	}

	@Override
	public List<Book> findAllByAuthor(Integer id) {
		return bookRepository.findAllByAuthor(id);
	}

}
