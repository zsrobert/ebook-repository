package ftn.udd.projekt.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.service.def.AuthorService;
import ftn.udd.projekt.service.impl.inMemory.InMemoryAuthorService;

public class InMemoryAuthorServiceTest {
	
	private AuthorService authorService;
	
	@Before
	public void setUp() {
		this.authorService = new InMemoryAuthorService();
		Author ivo = new Author();
		ivo.setName("Ivo Andric");
		
		Author patrik = new Author();
		patrik.setName("Patrik Rotfus");
		
		authorService.save(ivo);
		authorService.save(patrik);
	}
	
	@Test
	public void testfindOne() {
		Author author = authorService.findOne(1);
		Assert.assertNotNull(author);
		Assert.assertEquals("Ivo Andric", author.getName());
		Assert.assertNull(authorService.findOne(3));
	}

	@Test
	public void testFindAll() {
		List<Author> authors = authorService.findAll();
		
		Assert.assertEquals(2, authors.size());
		
		Author author1 = authors.get(0);
		Author author2 = authors.get(1);
		
		if (author1.getId().equals(1)) {
			Assert.assertEquals("Ivo Andric", author1.getName());
			
			Assert.assertTrue(author2.getId().equals(2) && "Patrik Rotfus".equals(author2.getName()));
		} else {
			Assert.assertEquals("Patrik Rotfus", author1.getName());
			
			Assert.assertTrue(author2.getId().equals(1) && "Ivo Andric".equals(author2.getName()));
		}
	}

	@Test
	public void testSave() {
		Author author = new Author();
		author.setName("Novi");
		
		author = authorService.save(author);
		
		Assert.assertNotNull(author);
		Assert.assertNotNull(author.getId());
		
		Assert.assertEquals("Novi", author.getName());
	}

	@Test
	public void testDelete() {
		Assert.assertNotNull(authorService.findOne(1));
		Assert.assertNotNull(authorService.findOne(2));
		
		authorService.delete(1);
		
		Assert.assertNull(authorService.findOne(1));
		Assert.assertNotNull(authorService.findOne(2));
	}

}
