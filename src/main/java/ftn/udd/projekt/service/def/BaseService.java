package ftn.udd.projekt.service.def;

import java.util.List;

import ftn.udd.projekt.model.BaseModel;

public interface BaseService<E extends BaseModel> {
	
	public E findOne(Integer id);
	public List<E> findAll();
	public E save(E item);
	public void delete(Integer id);
}
