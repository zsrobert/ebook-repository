package ftn.udd.projekt.lucene.indexer.handler;

import java.io.File;

import org.apache.lucene.document.Document;

import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.PDFBook;

public interface DocumentHandler {

	public Document getDocument(PDFBook book, Book bookModel);
	public Document getDocument(File file);

}
