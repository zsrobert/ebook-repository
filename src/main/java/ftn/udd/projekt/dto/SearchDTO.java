package ftn.udd.projekt.dto;

import org.apache.lucene.search.BooleanClause.Occur;

import ftn.udd.projekt.lucene.model.SearchType;

public class SearchDTO {
	private String authors;
	private Occur authorsOperator = Occur.SHOULD;
	private String keywords;
	private Occur keywordsOperator = Occur.SHOULD;
	private String text;
	private Occur textOperator = Occur.SHOULD;
	private String language;
	private Occur languageOperator = Occur.SHOULD;
	private String category;
	private Occur categoryOperator = Occur.SHOULD;
	private String title;
	private Occur titleOperator = Occur.SHOULD;
	private SearchType type;
	private boolean suggestDidYouMean = false;
	
	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public Occur getAuthorsOperator() {
		return authorsOperator;
	}

	public void setAuthorsOperator(Occur authorsOperator) {
		this.authorsOperator = authorsOperator;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Occur getKeywordsOperator() {
		return keywordsOperator;
	}

	public void setKeywordsOperator(Occur keywordsOperator) {
		this.keywordsOperator = keywordsOperator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Occur getTextOperator() {
		return textOperator;
	}

	public void setTextOperator(Occur textOperator) {
		this.textOperator = textOperator;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Occur getLanguageOperator() {
		return languageOperator;
	}

	public void setLanguageOperator(Occur languageOperator) {
		this.languageOperator = languageOperator;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Occur getCategoryOperator() {
		return categoryOperator;
	}

	public void setCategoryOperator(Occur categoryOperator) {
		this.categoryOperator = categoryOperator;
	}

	public SearchType getType() {
		return type;
	}

	public void setType(SearchType type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Occur getTitleOperator() {
		return titleOperator;
	}

	public void setTitleOperator(Occur titleOperator) {
		this.titleOperator = titleOperator;
	}

	public boolean isSuggestDidYouMean() {
		return suggestDidYouMean;
	}

	public void setSuggestDidYouMean(boolean suggestDidYouMean) {
		this.suggestDidYouMean = suggestDidYouMean;
	}
}
