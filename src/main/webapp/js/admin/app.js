var appConfig = {
	appRootPath : 'http://localhost:8080/UddProjekt/rest'
};

var UDD = window.UDD || {};

UDD.View = Backbone.View.extend({
	unset : function() {
		this.undelegateEvents();
		this.$el.removeData().unbind();
		this.$el.empty();
		this.stopListening();
	}
});

UDD.AppRouter = Backbone.Router.extend({
	routes : {
		"" : "authors",
		"authors" : "authors",
		"books" : "books",
		"categories" : "categories",
		"users" : "users"
	},
	authors : function() {
		var headerView = new UDD.HeaderView();
		headerView.render();

		var leftMenuView = new UDD.LeftMenuView({
			activeItem : "0"
		});
		leftMenuView.render();

		var fields = new UDD.AuthorFields();
		var collection = new UDD.Authors();

		var contentView = new UDD.ContentView({
			title : "class.author",
			headerCollection : fields,
			bodyCollection : collection
		});

		contentView.render();
	},
	books : function() {
		var headerView = new UDD.HeaderView();
		headerView.render();

		var leftMenuView = new UDD.LeftMenuView({
			activeItem : "1"
		});
		leftMenuView.render();

		var fields = new UDD.BookFields();
		var collection = new UDD.Books();

		var contentView = new UDD.ContentView({
			title : "class.book",
			editable : "true",
			imageUploadable : "true",
			headerCollection : fields,
			bodyCollection : collection,
			controllerView : UDD.AddBookView
		});

		contentView.render();
	},
	categories : function() {
		var headerView = new UDD.HeaderView();
		headerView.render();

		var leftMenuView = new UDD.LeftMenuView({
			activeItem : "2"
		});
		leftMenuView.render();

		var fields = new UDD.CategoryFields();
		
		UDD.getCategories(function(categories) {
			var contentView = new UDD.ContentView({
				title : "class.category",
				headerCollection : fields,
				bodyCollection : categories,
				controllerView : UDD.AddCategoryView
			});

			contentView.render();
		});

	},
	users : function() {
		var headerView = new UDD.HeaderView();
		headerView.render();

		var leftMenuView = new UDD.LeftMenuView({
			activeItem : "3"
		});
		leftMenuView.render();

		var fields = new UDD.UserFields();
		var collection = new UDD.Users();

		var contentView = new UDD.ContentView({
			title : "class.user",
			headerCollection : fields,
			bodyCollection : collection
		});

		contentView.render();
	}
});

Handlebars.registerHelper("ifCond", function(v1, operator, v2, options) {
	switch (operator) {
	case '==':
		return (v1 == v2) ? options.fn(this) : options.inverse(this);
	case '===':
		return (v1 === v2) ? options.fn(this) : options.inverse(this);
	case '<':
		return (v1 < v2) ? options.fn(this) : options.inverse(this);
	case '<=':
		return (v1 <= v2) ? options.fn(this) : options.inverse(this);
	case '>':
		return (v1 > v2) ? options.fn(this) : options.inverse(this);
	case '>=':
		return (v1 >= v2) ? options.fn(this) : options.inverse(this);
	case '&&':
		return (v1 && v2) ? options.fn(this) : options.inverse(this);
	case '||':
		return (v1 || v2) ? options.fn(this) : options.inverse(this);
	default:
		return options.inverse(this);
	}
});

Handlebars.registerHelper("i18n", function(v) {
	return Spring.messages[v];
});

Handlebars.registerHelper("eachProperty", function(context, options) {
	var keys = Object.keys(context);
	var i, len = keys.length;

	// sort to ensure same order on every pass
	keys.sort();

	// id should always be first
	var ret = options.fn({
		property : "id",
		value : context["id"]
	});

	for (i = 0; i < len; i++) {
		if (keys[i] != "id") {
			ret = ret + options.fn({
				property : keys[i],
				value : context[keys[i]]
			});
		}
	}

	return ret;
});

UDD.getCategories = function(callback) {
	if (UDD.categories) {
		callback(UDD.categories);
	} else {
		var categories = new UDD.Categories();
		categories.fetch({
			success : function(model, response, options) {
				UDD.categories = model;
				callback(model);
			},
			error : function(model, response, options) {
				console.log("Error fetching categories. Response: ");
				console.log(response);
			}
		});
	}
}
