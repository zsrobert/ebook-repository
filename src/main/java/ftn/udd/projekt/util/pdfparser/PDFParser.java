package ftn.udd.projekt.util.pdfparser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PDFParser extends org.apache.pdfbox.pdfparser.PDFParser {

	public PDFParser(InputStream input) throws IOException {
		super(input);
	}

	public PDFParser(byte[] input) throws IOException {
		super(new ByteArrayInputStream(input));
	}

}
