<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Login</title>

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
</head>
<body>
	<div id="container">
		<form class='form-signin' name='f'
			action="<c:url value='j_spring_security_check' />" method='POST'>
			<c:if test="${not empty error}">
				<div class="errorblock">
					${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
			</c:if>
			<h2 class="form-signin-heading">eBookRepository</h2>
			<input class="form-control" type="text" name='j_username' value=''
				placeholder="Username" required autofocus><input
				class="form-control" type="password" name='j_password'
				placeholder="Password" required> <input
				class="btn btn-lg btn-primary btn-block" type="submit" value="Login">
		</form>
	</div>
</body>
</html>






