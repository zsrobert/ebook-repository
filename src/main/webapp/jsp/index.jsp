<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>eBookRepository</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for login -->
<link href="css/login.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/bookstore.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="css/font-awesome.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
    <![endif]-->
</head>

<body>
	<div id="header"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-offset-1 col-xs-10 col-md-offset-2 col-md-8">
				<div id="left-menu" class="col-xs-3"></div>
				<div id="content" class="col-xs-9"></div>
			</div>
		</div>
	</div>
	<div id="modal"></div>

	<script type="text/template" id="register-modal-template">
	<div class="modal fade" id="registerModal" tabindex="-1" role="dialog"
		aria-labelledby="registerModallabe" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-border-bottom">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" >
						<div class="form-group">
							<label for="inputName" class="col-xs-2 control-label"><s:message code="form.label.name" /></label>
							<div class="col-xs-10">
								<input type="text" id="inputName" class="form-control" placeholder="<s:message code="form.label.name" />" />
							</div>
						</div>
						<div class="form-group">
							<label for="inputLastName" class="col-xs-2 control-label"><s:message code="form.label.lastname" /></label>
							<div class="col-xs-10">
								<input type="text" id="inputLastName" class="form-control" placeholder="<s:message code="form.label.lastname" />" />
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail" class="col-xs-2 control-label"><s:message code="form.label.email" /></label>
							<div class="col-xs-10">
								<input type="email" id="inputEmail" class="form-control" placeholder="<s:message code="form.label.email" />" />
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword" class="col-xs-2 control-label"><s:message code="form.label.password" /></label>
							<div class="col-xs-10">
								<input type="password" id="inputPassword" class="form-control" placeholder="<s:message code="form.label.password" />" />
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<div class="row">
            			<div class="errorblock bg-danger text-danger hidden"></div>
        			</div>
					<div class="row">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><s:message code="form.button.close" /></button>
						<button type="button" class="btn btn-success" id="register-button"><s:message code="form.button.register" /></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>

	<script type="text/template" id="index-template">
	<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#register-modal">
		<s:message code="form.button.register" />
	</button>
	</script>

	<script type="text/template" id="header-template">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="col-xs-offset-1 col-xs-10 col-md-offset-2 col-md-8">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">eBookRepository</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="loggedInCheck"><a><i class="fa fa-cog fa-spin"></i></a></li>
					<li class="hidden loggedOutItem"><a id="signinAnchor"><s:message code="text.bookstore.signin" /></a></li>
					<li class="hidden loggedOutItem"><a id="registerAnchor"><s:message code="text.bookstore.register" /></a></li>
					<li class="hidden loggedInItem"><a id="userAnchor"><span class="glyphicon glyphicon-user"></span></a></li>
					<li class="hidden adminItem"><a href="admin" id="logoutAnchor"><span class="glyphicon glyphicon-wrench"></span></a></li>
					<li class="hidden loggedInItem"><a href="j_spring_security_logout" id="logoutAnchor"><span class="glyphicon glyphicon-log-out"></span></a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#advanced"><s:message code="form.button.advancedsearch" /></a></li>
				</ul>
				<form class="navbar-form navbar-right" role="search">
					<div class="input-group">
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-search"></span></span> <input type="text"
							class="form-control input-enlarge"
							placeholder="<s:message code="form.placeholder.search" />"
							id="search-input" /> <span class="input-group-btn">
								<button type="button" class="btn btn-primary" id="search-button">
									<s:message code="form.button.search" />
								</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</nav>
			</script>

	<script type="text/template" id="left-menu-template">
	<ul class="list-group">
		<li class="list-group-item list-group-header"><s:message code="menu.bookstore.title" /></li>
		<li class="list-group-item list-group-link-item {{#ifCond selectedId '===' -1}}active{{/ifCond}}"><a href="#"><s:message code="text.bookstore.allbooks" /></a></li>
		{{#each items}}
			<li class="list-group-item list-group-link-item {{#ifCond selectedId '==' this.id}}active{{/ifCond}}"><a href="#category/{{this.id}}">{{this.name}}</a></li>
		{{/each}}
	</ul>
	</script>

	<script type="text/template" id="category-books-template">
	<div class="col-xs-12">
		<h2 class="pull-left no-margin-top margin-bottom">{{categoryTitle}}</h2>
		<div class="pull-right">
      		<span class="input-group-addon view-control" id="view-grid">
        		<span class="glyphicon glyphicon-th"></span>
     		</span>
			<span class="input-group-addon view-control" id="view-list">
        		<span class="glyphicon glyphicon-list"></span>
     		</span>
    	</div>
	</div>
	<div class="mix-container">
	{{#each items}}
		<div class="mix book-item" id="{{this.id}}">
			<div class="panel panel-default panel-clickable">
				<div class="panel-heading text-center transparent-background">
					<img class="book-image" src="books/image/{{this.id}}" alt="{{this.title}}" />
				</div>
				<div class="panel-body">
					<h4 class="book-title">{{this.title}}</h4>
					<a class="category-link" href="#category/{{this.category.id}}">{{this.category.name}}</a>
					<div class="book-description">{{this.description}}</div>
				</div>
			</div>
		</div>
	{{/each}}
	</div>
	</script>
	<script type="text/template" id="search-results-template">
	<div class="col-xs-12">
		<h2 class="pull-left no-margin-top margin-bottom">
			<s:message code="text.bookstore.searchresults" />
		</h2>
	</div>
	{{#if didYouMean}}
		<div class="col-xs-12 margin-bottom">
			<span class="text-danger"><s:message code="text.bookstore.didyoumean" />:</span> <a href="" id="didYouMeanAnchor">{{didYouMean}}</a>
		</div>
	{{/if}}
	<div class="list">
		{{#each items}}
		<div class="book-item" id="{{this.id}}">
			<div class="panel panel-default">
				<div class="panel-heading text-center transparent-background">
					<img class="book-image" src="books/image/{{this.id}}" alt="{{this.title}}" />
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="book-title pull-left">
								<span class="title-string">{{this.title}} &nbsp;</span>
							</h4>
							<a class="category-link pull-left" href="#similar/{{this.id}}">More like this</a>
						</div>
					</div>
					<a class="category-link" href="#category/{{this.category.id}}">{{this.category.name}}</a>
					<div class="book-description panel-clickable">{{{this.highlightedText}}}</div>
				</div>
			</div>
		</div>
		{{/each}}
	</div>
	</script>

	<script type="text/template" id="book-info-template">
		<div class="thumbnail">
			<img class="book-image" src="books/image/{{item.id}}" alt="{{item.title}}" />
		</div>
		<div class="well">
			<div>
				<i><s:message code="text.bookstore.publicationdate" />:</i><br />
				<div class="text-right">{{item.publishingDate}}</div>
			</div>
			<br />
			<div>
				<i><s:message code="text.bookstore.authors" />:</i><br />
				<div class="text-right">
					{{#each item.authors}}
						<a class="author-link" href="#author/{{this.id}}">{{this.name}}</a><span>, </span>
					{{/each}}
				</div>
			</div>
		</div>
		<div>
			<sec:authorize access="isAuthenticated()">
				<button class="btn btn-lg btn-success col-xs-12" id="downloadButton">Download</button>
			</sec:authorize>
		</div>
	</script>

	<script type="text/template" id="book-content-template">
		<div class="container-fluid">
			<div class="row well">
				<div class="col-xs-12">
					<a class="category-link" href="#category/{{item.category.id}}">{{item.category.name}}</a>
				</div>
				<div class="col-xs-12">
					<h2 class="no-margin-top margin-bottom">{{item.title}}</h2>
				</div>
				<div class="col-xs-12">{{item.description}}</div>
			</div>
			<div class="row" id="suggested-books"></div>
		</div>
	</script>

	<script type="text/template" id="suggested-books-template">

		<div class="row">
			<div class="col-xs-12"><h4 class="margin-bottom"><s:message code="text.bookstore.alsoviewed" />:</h4></div>
			{{#each items}}
				<div class="col-xs-3" id="{{this.id}}">
					<div class="well">
						<div class="thumbnail text-center book-image-suggestion">
							<img width="85" height="117" src="books/image/{{this.id}}" alt="{{this.title}}" />
						</div>
						<div class="book-link" class="text-center">
							<a href="#book/{{this.id}}">{{this.title}}</a>
						</div>
					</div>
				</div>
			{{/each}}
		</div>
	</script>

	<script type="text/template" id="advanced-search-template">
	<div class="well">
		<form class="form-horizontal" role="form">
			<div class="form-group">
				<label for="searchType"
					class="col-xs-2 control-label  control-label-left"><s:message
						code="form.label.search.type" /></label>
				<div class="col-xs-3">
					<select class="form-control" id="searchType">
						<option value="REGULAR" selected>regular</option>
						<option value="FUZZY">fuzzy</option>
						<option value="PHRASE">phrase</option>
						<option value="RANGE">range</option>
						<option value="PREFIX">prefix</option>
					</select>
				</div>
				<div class="col-xs-7"></div>
			</div>
			<div class="form-group">
				<label for="searchTitle"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.title" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="titleOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchTitle" class="form-control"
						placeholder="<s:message code="form.label.book.title" />" />
				</div>
			</div>
			<div class="form-group">
				<label for="searchText"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.text" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="textOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchText" class="form-control"
						placeholder="<s:message code="form.label.book.text" />" />
				</div>
			</div>
			<div class="form-group">
				<label for="searchAuthors"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.authors" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="authorsOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchAuthors" class="form-control"
						placeholder="<s:message code="form.label.book.authors" />" />
				</div>
			</div>
			<div class="form-group">
				<label for="searchLanguage"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.language" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="languageOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchLanguage" class="form-control"
						placeholder="<s:message code="form.label.book.language" />" />
				</div>
			</div>
			<div class="form-group">
				<label for="searchKeywords"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.keywords" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="keywordsOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchKeywords" class="form-control"
						placeholder="<s:message code="form.label.book.keywords" />" />
				</div>
			</div>
			<div class="form-group">
				<label for="searchCategory"
					class="col-xs-2 control-label control-label-left"><s:message
						code="form.label.book.category" /></label>
				<div class="col-xs-2">
					<select class="form-control" id="categoryOperator">
						<option value="MUST">and</option>
						<option value="SHOULD" selected>or</option>
						<option value="MUST_NOT">and not</option>
					</select>
				</div>
				<div class="col-xs-8">
					<input type="text" id="searchCategory" class="form-control"
						placeholder="<s:message code="form.label.book.category" />" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<button class="btn btn-primary pull-right"
						id="advanced-search-button">
						<s:message code="form.button.advancedsearch" />
					</button>
				</div>
			</div>
		</form>
	</div>
	</script>

	<script type="text/template" id="signin-modal-template">
	<div class="modal fade" id="signinModal" tabindex="-1" role="dialog"
		aria-labelledby="signinModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-border-bottom">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
				</div>
				<div class="modal-body">
					<form class='form-signin' name='f' method='POST'>
						<div class="errorblock bg-danger text-danger hidden"></div>
						<input class="form-control" type="text" name='j_username' value=''
							placeholder="E-mail" id="signinEmail" required autofocus><input
							class="form-control" type="password" name='j_password'
							placeholder="Password" id="signinPassword" required> <input id="signinButton"
							class="btn btn-lg btn-primary btn-block" type="button"
							value="<s:message code="text.bookstore.signin" />">
					</form>
				</div>
			</div>
		</div>
	</div>
	</script>
	
	<script type="text/template" id="subscribe-modal-template">
	<div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog"
		aria-labelledby="subscribeModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header no-border-bottom">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<button class="btn btn-lg btn-primary col-xs-offset-1 col-xs-10 margin-bottom" id="categorySubButton">Subscribe to {{category.name}}</button>
						<button class="btn btn-lg btn-primary col-xs-offset-1 col-xs-10" id="allSubButton">Subscribe to all</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>
	
	<script type="text/template" id="user-template">
	<div class="row margin-bottom">
		<div class="col-xs-2 text-right"><strong><s:message code="text.bookstore.language" /></strong></div>
		<div class="col-xs-2"><a href="?lang=en"><strong>English</strong></a></div>
		<div class="col-xs-2"><a href="?lang=sr"><strong>Srpski</strong></a></div>
	</div>
	<form class="form-horizontal" role="form">
		<input type="hidden" id="inputId" value="{{user.id}}" />
		<div class="form-group">
			<label for="inputName" class="col-xs-2 control-label"><s:message
					code="form.label.name" /></label>
			<div class="col-xs-10">
				<input type="text" id="inputName" class="form-control user-control"
					placeholder="<s:message code="form.label.name" />"
					value="{{user.firstName}}" disabled />
			</div>
		</div>
		<div class="form-group">
			<label for="inputLastName" class="col-xs-2 control-label"><s:message
					code="form.label.lastname" /></label>
			<div class="col-xs-10">
				<input type="text" id="inputLastName" class="form-control user-control"
					placeholder="<s:message code="form.label.lastname" />"
					value="{{user.lastName}}" disabled />
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail" class="col-xs-2 control-label"><s:message
					code="form.label.email" /></label>
			<div class="col-xs-10">
				<input type="email" id="inputEmail" class="form-control user-control"
					placeholder="<s:message code="form.label.email" />"
					value="{{user.email}}" disabled />
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-xs-2 control-label"><s:message
					code="form.label.password" /></label>
			<div class="col-xs-10">
				<input type="password" id="inputPassword" class="form-control user-control"
					placeholder="<s:message code="form.label.password" />"
					value="{{user.password}}" disabled />
			</div>
		</div>
		<div class="form-group">
			<label for="inputPassword" class="col-xs-2 control-label"><s:message
					code="form.label.subscription" /></label>
			<div class="col-xs-10">
				<select id="inputSubscription" class="form-control user-control" disabled>
					<option value="-1" {{#ifCond '-1' '==' user.subscribedTo.id}}selected{{/ifCond}}>None</option>
					<option value="0" {{#ifCond '0' '==' user.subscribedTo.id}}selected{{/ifCond}}>All</option>
					{{#each categories}}
						<option value="{{this.id}}" {{#ifCond this.id '==' ../user.subscribedTo.id}}selected{{/ifCond}}>{{this.name}}</option>
					{{/each}}
				</select>
			</div>
		</div>
	</form>
	<div id="inputErrorTextHolder" class="text-right col-xs-12 margin-bottom">

	</div>
	<button class="btn btn-primary pull-right" id="editProfileButton"><s:message code="form.button.editprofile" /></button>
	<button class="btn btn-success hidden pull-right" id="saveProfileButton"><s:message code="form.button.save" /></button>
	</script>

	<script type="text/javascript">
		var Spring = window.Spring || {};

		Spring.securityUrl = "<c:url value='j_spring_security_check' />";
		Spring.securityLogoutUrl = "<c:url value='j_spring_security_logout' />";

		Spring.messages = {
			"error.text.authenticationFailed" : "<s:message code='error.text.authenticationFailed' />",
			"text.bookstore.allbooks" : "<s:message code='text.bookstore.allbooks'/>",
			"error.input.user.firstName" : "<s:message code='error.input.user.firstName' />",
			"error.input.user.lastName" : "<s:message code='error.input.user.lastName' />",
			"error.input.user.email" : "<s:message code='error.input.user.email' />",
			"error.input.user.password" : "<s:message code='error.input.user.password' />",
			"error.input.user.password.length" : "<s:message code='error.input.user.password.length' />"
		};
	</script>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/underscore.js"></script>
	<script src="js/handlebars.js"></script>
	<script src="js/backbone.js"></script>
	<script src="js/bookstore/app.js"></script>
	<script src="js/bookstore/backbone.models.js"></script>
	<script src="js/bookstore/backbone.views.js"></script>
	<script src="js/backbone.start.js"></script>
	<script src="js/bookstore/jquery.mixitup.min.js"></script>
	<script src="js/bookstore/ui-addons.js"></script>
</body>
</html>
