<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Dashboard for eBookRepository</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/admin.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="css/font-awesome.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
    <![endif]-->
</head>

<body>
	<div id="header"></div>
	<div class="container-fluid">
		<div class="row">
			<div id="left-menu"></div>
			<div id="content"></div>
		</div>
	</div>
	<div id="modal"></div>


	<script type="text/template" id="header-template">
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">eBookRepository</a>
            </div>
            <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
               		<li><a href="UddProjekt/j_spring_security_logout"><strong><s:message code="menu.admin.logout" /></strong></a></li>
				</ul>
				<ul class="nav navbar-nav">
               		<li><a href="?lang=rs"><strong>EN</strong></a></li>
					<li><a href="?lang=sr"><strong>SR</strong></a></li>
				</ul>
            </div>
        </div>
    </div>
</script>
	<script type="text/template" id="left-menu-template">
    <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
            <li {{#ifCond activeItem '==' '0'}} class="active" {{/ifCond}} ><a href="#authors" id="authors-link"><s:message code="menu.admin.authors" /></a></li>
            <li {{#ifCond activeItem '==' '1'}} class="active" {{/ifCond}} ><a href="#books" id="books-link"><s:message code="menu.admin.books" /></a></li>
            <li {{#ifCond activeItem '==' '2'}} class="active" {{/ifCond}} ><a href="#categories" id="categories-link"><s:message code="menu.admin.categories" /></a></li>
            <li {{#ifCond activeItem '==' '3'}} class="active" {{/ifCond}} ><a href="#users" id="users-link"><s:message code="menu.admin.users" /></a></li>
        </ul>
    </div>
</script>
	<script type="text/template" id="content-template">
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="alert alert-success alert-dismissable display-none">
  			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  			<span id="alert-text"></span>
		</div>
        <h2 class="sub-header">{{i18n title}}</h2>
        <div id="content-control"></div>
        <div class="table-responsive">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr id="content-table-header">

                    </tr>
                </thead>
                <tbody id="content-table-body">
                </tbody>
            </table>
        </div>
    </div>
</script>
	<script type="text/template" id="content-table-header-template">
    {{#each fields}}
        <th>{{i18n name}}</th>
    {{/each}}
        <th></th>
		{{#ifCond imageUploadable '==' 'true'}}<th></th>{{/ifCond}}
		{{#ifCond editable '==' 'true'}}<th></th>{{/ifCond}}
</script>
	<script type="text/template" id="content-table-body-template">
    {{#each items}}
        <tr>
        	{{#eachProperty this}}
         	   <td>{{#if value.name }} {{value.name}} {{else}} {{value}} {{/if}}</td>
        	{{/eachProperty}}
            {{#ifCond ../imageUploadable '==' 'true'}}<td>
					<strong><span class="btn-img glyphicon glyphicon-picture cornflower-blue fileinput-button" id="img-{{id}}">
 						<input id="fileupload" type="file" name="imageFile" />		
					</span></strong>
			</td>{{/ifCond}}
			{{#ifCond ../editable '==' 'true'}}<td><strong><span class="btn-edt glyphicon glyphicon-edit sea-green" id="edt-{{id}}"></span></strong></td>{{/ifCond}}
			<td><strong><span class="btn-rst glyphicon glyphicon-remove dark-red" id="rst-{{id}}"></span></strong></td>
        </tr>
    {{/each}}
</script>
	<script type="text/template" id="category-content-control-template">
    <div class="">
        <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control col-md-4" name="category-name" placeholder="<s:message code="form.placeholder.newcategoryname" />" />
            </div>
            <div class="col-md-3">
                <button class="btn btn-success" id="add-category-button"><s:message code="form.button.add" /></button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-top-sm" id="input-error-text-holder">
                &nbsp;
            </div>
        </div>
    </div>
</script>
	<script type="text/template" id="book-content-control-template">
	<div class="container-fluid">
		<div class="row">
			<span class="btn btn-success fileinput-button"> <span><s:message
						code="text.admin.uploadbook" /></span> <input id="fileupload"
				type="file" name="file">
			</span>
		</div>
		<div class="row">
			<div class="progress progress-striped active invisible margin-vertical-sm">
				<div id="upload-progress-bar" class="progress-bar"
					role="progressbar" aria-valuenow="0" aria-valuemin="0"
					aria-valuemax="100"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 margin-top-sm" id="input-error-text-holder">
				&nbsp;</div>
		</div>
	</div>
	</script>
	<script type="text/template" id="add-book-modal-template">
	<div class="modal fade" id="add-book-modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">
						<s:message code="text.admin.addbook" />
					</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form">
						<div class="form-group">
							<label for="bookId" class="col-xs-2 control-label"><s:message
									code="form.label.book.id" /></label>
							<div class="col-xs-10">
								<input type="text" id="bookId" class="form-control"
									placeholder="<s:message code="form.label.book.id" />" value="{{item.id}}" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label for="bookTitle" class="col-xs-2 control-label"><s:message
									code="form.label.book.title" /></label>
							<div class="col-xs-10">
								<input type="text" id="bookTitle" class="form-control"
									placeholder="<s:message code="form.label.book.title" />" value="{{item.title}}" required/>
							</div>
						</div>
						<div class="form-group">
							<label for="bookAuthors" class="col-xs-2 control-label"><s:message
									code="form.label.book.authors" /></label>
							<div class="col-xs-10">
								<input type="text" id="bookAuthors" class="form-control"
									placeholder="<s:message code="form.label.book.authors" />" value="{{item.authors}}" />
							</div>
						</div>
						<div class="form-group">
							<label for="bookLanguage" class="col-xs-2 control-label"><s:message
									code="form.label.book.language" /></label>
							<div class="col-xs-10">
								<input type="text" id="bookLanguage" class="form-control"
									placeholder="<s:message code="form.label.book.language" />" value="{{item.language}}" />
							</div>
						</div>
						<div class="form-group">
							<label for="bookKeywords" class="col-xs-2 control-label"><s:message
									code="form.label.book.keywords" /></label>
							<div class="col-xs-10">
								<input type="text" id="bookKeywords" class="form-control"
									placeholder="<s:message code="form.label.book.keywords" />" value="{{item.keyWords}}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="bookDate" class="col-xs-2 control-label"><s:message
									code="form.label.book.publishingdate" /></label>
							<div class="col-xs-10">
								<input type="date" id="bookDate" class="form-control"
									placeholder="<s:message code="form.label.book.publishingdate" />" value="{{item.publishingDate}}"/>
							</div>
						</div>
						<div class="form-group">
							<label for="bookDescription" class="col-xs-2 control-label"><s:message
									code="form.label.book.description" /></label>
							<div class="col-xs-10">
								<textarea id="bookDescription" class="form-control"
									placeholder="<s:message code="form.label.book.description" />" >{{item.description}}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="bookCategory" class="col-xs-2 control-label"><s:message
									code="form.label.book.category" /></label>
							<div class="col-xs-10">
								<select id="bookCategory" class="form-control">
									{{#each categories}}
										<option value="{{this.id}}" {{#ifCond this.id '==' ../item.category.id}}selected{{/ifCond}}>{{this.name}}</option>
									{{/each}}
								</select>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="errorblock bg-danger text-danger hidden"></div>
					</div>
					<div class="row">
						<button type="button" id="cancel-button" class="btn btn-primary" data-dismiss="modal">
							<s:message code="form.button.cancel" />
						</button>
						<button type="button" class="btn btn-success" id="save-button">
							<s:message code="form.button.save" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</script>

	<script type="text/javascript">
		var Spring = window.Spring || {};

		Spring.messages = {
			"field.any.id" : "<s:message code='field.any.id' />",
			"field.author.name" : "<s:message code='field.author.name' />",
			"field.book.title" : "<s:message code='field.book.title' />",
			"field.book.authors" : "<s:message code='field.book.authors' />",
			"field.book.description" : "<s:message code='field.book.description' />",
			"field.book.keyWords" : "<s:message code='field.book.keyWords' />",
			"field.book.language" : "<s:message code='field.book.language' />",
			"field.book.publishingDate" : "<s:message code='field.book.publishingDate' />",
			"field.book.category" : "<s:message code='field.book.category' />",
			"field.category.name" : "<s:message code='field.category.name' />",
			"field.user.firstName" : "<s:message code='field.user.firstName' />",
			"field.user.lastName" : "<s:message code='field.user.lastName' />",
			"field.user.admin" : "<s:message code='field.user.admin' />",
			"field.user.email" : "<s:message code='field.user.email' />",
			"field.user.password" : "<s:message code='field.user.password' />",
			"field.user.subscribedTo" : "<s:message code='field.user.subscribedTo' />",
			"class.author" : "<s:message code='class.author' />",
			"class.book" : "<s:message code='class.book' />",
			"class.category" : "<s:message code='class.category' />",
			"class.user" : "<s:message code='class.user' />",
			"error.input.category.name" : "<s:message code='error.input.category.name' />",
			"error.input.user.firstName" : "<s:message code='error.input.user.firstName' />",
			"error.input.user.lastName" : "<s:message code='error.input.user.lastName' />",
			"error.input.user.email" : "<s:message code='error.input.user.email' />",
			"error.input.user.password" : "<s:message code='error.input.user.password' />",
			"error.input.user.password.length" : "<s:message code='error.input.user.password.length' />",
			"error.input.book.title" : "<s:message code='error.input.book.title' />",
			"error.input.book.description" : "<s:message code='error.input.book.description' />",
			"error.input.book.language" : "<s:message code='error.input.book.language' />",
			"error.input.book.category" : "<s:message code='error.input.book.category' />",
			"error.input.book.authors" : "<s:message code='error.input.book.authors' />",
			"error.input.book.publishingDate" : "<s:message code='error.input.book.publishingDate' />",
			"error.input.book.publishingDate.format" : "<s:message code='error.input.book.publishingDate.format' />",
			"error.input.author.name" : "<s:message code='error.input.author.name' />"
		};
	</script>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/underscore.js"></script>
	<script src="js/handlebars.js"></script>
	<script src="js/backbone.js"></script>
	<script src="js/admin/app.js"></script>
	<script src="js/admin/backbone.models.js"></script>
	<script src="js/admin/backbone.views.js"></script>
	<script src="js/admin/jquery.ui.widget.js"></script>
	<script src="js/admin/jquery.iframe-transport.js"></script>
	<script src="js/admin/jquery.fileupload.js"></script>
	<script src="js/backbone.start.js"></script>
</body>
</html>
