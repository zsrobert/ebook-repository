package ftn.udd.projekt.model;

public class PDFBook extends BaseModel {
	private byte[] fileBytes;

	public PDFBook(Integer id, byte[] bytes) {
		this.id = id;
		this.fileBytes = bytes;
	}

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
}
