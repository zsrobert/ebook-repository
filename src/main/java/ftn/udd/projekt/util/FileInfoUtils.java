package ftn.udd.projekt.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.springframework.web.multipart.MultipartFile;

import ftn.udd.projekt.dto.BookAdminDTO;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.PDFBook;
import ftn.udd.projekt.util.pdfparser.PDFParser;

public class FileInfoUtils {

	public static BookAdminDTO parseBookModelFromFile(MultipartFile file) {
		PDFParser parser;
		BookAdminDTO book = null;

		try {
			parser = new PDFParser(file.getBytes());
			parser.parse();
			PDDocument pdf = parser.getPDDocument();
			PDDocumentInformation info = pdf.getDocumentInformation();

			book = new BookAdminDTO();

			book.setTitle("" + info.getTitle());
			book.setDescription("" + info.getCustomMetadataValue("description"));
			book.setKeyWords("" + info.getKeywords());
			book.setAuthors("" + info.getAuthor());
			pdf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return book;
	}

	public static PDFBook setPDFInfo(PDFBook book, Book bookModel) {
		PDFParser parser;
		PDFBook returnBook = null;
		try {
			parser = new PDFParser(book.getFileBytes());
			parser.parse();
			PDDocument pdf = parser.getPDDocument();
			PDDocumentInformation info = pdf.getDocumentInformation();
			info.setAuthor(getCommaSeparatedList(bookModel.getAuthors()));
			info.setTitle(bookModel.getTitle());
			info.setKeywords(getCommaSeparatedList(bookModel.getKeyWords()));
			info.setCustomMetadataValue("description",
					bookModel.getDescription());
			info.setCustomMetadataValue("language", bookModel.getLanguage());
			pdf.setDocumentInformation(info);
			returnBook = new PDFBook(book.getId(), toByteArray(pdf));
			pdf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnBook;
	}

	private static String getCommaSeparatedList(Set<?> set) {
		return StringUtils.join(set, ", ");
	}
	
	private static String getCommaSeparatedList(List<?> list) {
		return StringUtils.join(list, ", ");
	}
	
	private static byte[] toByteArray(PDDocument doc) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			doc.save(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return out.toByteArray();
	}
}
