package ftn.udd.projekt.service.impl.inMemory;

import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.PDFBook;
import ftn.udd.projekt.service.def.PDFBookService;

@Service
public class InMemoryPDFBookService extends InMemoryBaseService<PDFBook> implements PDFBookService {

}
