package ftn.udd.projekt.dto;

import java.io.Serializable;

import ftn.udd.projekt.model.Category;

public class CategoryDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 1747412248343710379L;
	
	private String name;

	public CategoryDTO (Category category) {
		this.id = category.getId();
		this.name = category.getName();
	}
	
	public CategoryDTO() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
