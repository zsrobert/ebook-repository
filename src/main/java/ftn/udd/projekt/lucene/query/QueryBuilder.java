package ftn.udd.projekt.lucene.query;

import java.util.StringTokenizer;

import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import ftn.udd.projekt.lucene.analyzer.SerbianAnalyzer;
import ftn.udd.projekt.lucene.model.SearchType;

public class QueryBuilder {

	private static Version VERSION = Version.LUCENE_48;
	private static SerbianAnalyzer ANALYZER = new SerbianAnalyzer(VERSION);
	private static int MAX_EDITS = 1;

	public static int getMaxEdits() {
		return MAX_EDITS;
	}

	public static void setMaxEdits(int maxEdits) {
		QueryBuilder.MAX_EDITS = maxEdits;
	}

	public static Query buildQuery(SearchType queryType, String field,
			String value) {
		QueryParser parser = new QueryParser(VERSION, field, ANALYZER);

		Query query = null;
		if (queryType.equals(SearchType.REGULAR)) {
			Term term = new Term(field, value.trim());
			query = new TermQuery(term);
		} else if (queryType.equals(SearchType.FUZZY)) {
			Term term = new Term(field, value.trim());
			query = new FuzzyQuery(term, MAX_EDITS);
		} else if (queryType.equals(SearchType.PREFIX)) {
			Term term = new Term(field, value.trim());
			query = new PrefixQuery(term);
		} else if (queryType.equals(SearchType.RANGE)) {
			StringTokenizer st = new StringTokenizer(value.trim());
			if (st.countTokens() < 2) {
				query = new TermQuery(new Term(field, value.trim()));
			} else {
				query = new TermRangeQuery(field, new BytesRef(st.nextToken()),
						new BytesRef(st.nextToken()), true, true);
			}
		} else {
			StringTokenizer st = new StringTokenizer(value.trim());
			PhraseQuery pq = new PhraseQuery();
			while (st.hasMoreTokens()) {
				pq.add(new Term(field, st.nextToken()));
			}
			query = pq;
		}

		try {
			return parser.parse(query.toString(field));
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

}
