package ftn.udd.projekt.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.udd.projekt.dto.AuthorDTO;
import ftn.udd.projekt.dto.FieldDTO;
import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.service.def.AuthorService;
import ftn.udd.projekt.util.ClassFieldUtils;

@RestController
public class AuthorsController {
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/rest/authors", method = RequestMethod.GET)
	public List<AuthorDTO> handleGetAuthor() {
		List<Author> authors = authorService.findAll();
		List<AuthorDTO> response = new ArrayList<AuthorDTO>();

		for (Author author : authors) {
			response.add(new AuthorDTO(author));
		}

		return response;
	}

	@RequestMapping(value = "/rest/authors/{id}", method = RequestMethod.GET)
	public ResponseEntity<AuthorDTO> handleGetAuthor(@PathVariable Integer id) {
		Author author = authorService.findOne(id);
		ResponseEntity<AuthorDTO> response;

		if (author != null) {
			response = new ResponseEntity<AuthorDTO>(new AuthorDTO(author),
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<AuthorDTO>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/rest/authors/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<AuthorDTO> handleDeleteAuthor(@PathVariable Integer id) {
		Author author = authorService.findOne(id);
		ResponseEntity<AuthorDTO> response;

		if (author != null) {
			authorService.delete(id);
			response = new ResponseEntity<>(new AuthorDTO(author), HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/rest/authors", method = RequestMethod.POST)
	public ResponseEntity<AuthorDTO> handlePostAuthor(
			@RequestBody @Valid AuthorDTO authorDTO) {
		Author author = new Author(authorDTO);
		ResponseEntity<AuthorDTO> response;

		author = authorService.save(author);

		if (author != null && author.getId() != null) {
			response = new ResponseEntity<AuthorDTO>(new AuthorDTO(author),
					HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}

	@RequestMapping(value = "/rest/authors/fields", method = RequestMethod.GET)
	public List<FieldDTO> handleFieldsGet() {
		return ClassFieldUtils.getFieldDTOs(AuthorDTO.class);
	}
}
