package ftn.udd.projekt.service.def;

import ftn.udd.projekt.model.Category;

public interface CategoryService extends BaseService<Category> {

}
