package ftn.udd.projekt.service.def;

import ftn.udd.projekt.model.Author;

public interface AuthorService extends BaseService<Author> {
	public Author findOneByName(String name);
}
