package ftn.udd.projekt.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.ELRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

@Component(value = "ajaxAuthenticationFailureHandler")
public class AjaxAuthenticationFailureHandler extends
		SimpleUrlAuthenticationFailureHandler {

	private static final RequestMatcher REQUEST_MATCHER = new ELRequestMatcher(
			"hasHeader('X-Requested-With','XMLHttpRequest')");

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		if (isAjaxRequest(request)) {
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			// response.sendError(HttpStatus.UNAUTHORIZED.value(),
			// "error.text.authenticationFailed");
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.getWriter().write(
					"{ \"message\" : \"error.text.authenticationFailed\" }");
		} else {
			super.onAuthenticationFailure(request, response, exception);
		}
	}

	private boolean isAjaxRequest(HttpServletRequest request) {
		return REQUEST_MATCHER.matches(request);
	}

}
