UDD.Category = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/categories/",

	defaults : {
		id : undefined,
		name : "Unknown"
	},

	validate : function(attrs) {
		if (!attrs.name || attrs.name.trim() == "") {
			return "Please enter a name for the Category.";
		}
	}
});

UDD.User = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/users/",

	defaults : {
		id : undefined,
		firstName : "Unknown",
		lastName : "Unknown",
		email : "Unknown",
		password : "Unknown"
	},

	validate : function(attrs) {
		if (!attrs.firstName || attrs.firstName.trim() == "") {
			return Spring.messages["error.input.user.firstName"];
		}
		if (!attrs.lastName || attrs.lastName.trim() == "") {
			return Spring.messages["error.input.user.lastName"];
		}
		if (!attrs.email || attrs.email.trim() == "") {
			return Spring.messages["error.input.user.email"];
		}
		if (!attrs.password || attrs.password.trim() == "") {
			return Spring.messages["error.input.user.password"];
		} else {
			if (attrs.password.length < 5) {
				return Spring.messages["error.input.user.password.length"];
			}
		}
	}
});

UDD.Book = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/books/",

	defaults : {
		id : undefined,
		title : "Unknown",
		description : "Unknown",
		language : "Unknown",
		publishingDate : "Unknown",
		category : "Unknown",
		authors : "Unknown",
		keyWords : "Unknown"
	},

	validate : function(attrs) {
		return "Why are you uploading a book?";
	}
});

UDD.SearchResult = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/books/search",

	defaults : {
		id : undefined,
		title : "Unknown",
		description : "Unknown",
		language : "Unknown",
		publishingDate : "Unknown",
		category : undefined,
		authors : undefined,
		keyWords : undefined,
		highlight : undefined
	}
});

UDD.Author = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/authors/",

	defaults : {
		id : undefined,
		name : "Unknown"
	},

	validate : function(attrs) {
		if (!attrs.name || attrs.name.trim() == "") {
			return "An author cannot be nameless!.";
		}
	}
});

UDD.Categories = Backbone.Collection.extend({
	model : UDD.Category,
	url : appConfig.appRootPath + "/categories/"
});

UDD.Users = Backbone.Collection.extend({
	model : UDD.User,
	url : appConfig.appRootPath + "/users/"
});

UDD.Authors = Backbone.Collection.extend({
	model : UDD.Author,
	url : appConfig.appRootPath + "/authors/"
});

UDD.Books = Backbone.Collection.extend({
	model : UDD.Book,
	url : appConfig.appRootPath + "/books/"
});

UDD.CategoryBooks = Backbone.Collection.extend({
	model : UDD.Book,
	initialize : function(options) {
		this.options = options || {};
		this.id = options.id;
	},
	setId : function(id) {
		this.id = id;
	},
	url : function() {
		return appConfig.appRootPath + "/books/categories/" + this.id;
	}
});

UDD.AuthorBooks = Backbone.Collection.extend({
	model : UDD.Book,
	initialize : function(options) {
		this.options = options || {};
		this.id = options.id;
	},
	setId : function(id) {
		this.id = id;
	},
	url : function() {
		return appConfig.appRootPath + "/books/authors/" + this.id;
	}
});

UDD.SimilarBooks = Backbone.Collection.extend({
	model : UDD.Book,
	initialize : function(options) {
		this.options = options || {};
		this.id = options.id;
	},
	setId : function(id) {
		this.id = id;
	},
	url : function() {
		return appConfig.appRootPath + "/books/similar/" + this.id;
	}
});

UDD.SuggestedBooks = Backbone.Collection.extend({
	model : UDD.Book,
	initialize : function(options) {
		this.options = options || {};
		this.id = options.id;
	},
	setId : function(id) {
		this.id = id;
	},
	url : function() {
		return appConfig.suggestionsRootPath + "/suggestions/" + this.id;
	}
});

UDD.SearchResults = Backbone.Collection.extend({
	model : UDD.SearchResult,
	initialize : function(options) {
		this.options = options || {};
		this.query = options.query;
	},
	setQuery : function(query) {
		this.query = query;
	},
	url : function() {
		return appConfig.appRootPath + "/books/search" + "?query="
				+ this.query;
	}, 
	parse : function(response) {
		if (response.data) {
			this.didYouMean = response.didYouMean;
			return response.data;
		} else {
			return response;
		}
	}
});
