package ftn.udd.projekt.email.service.impl;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import ftn.udd.projekt.email.EmailWorker;
import ftn.udd.projekt.email.job.SendMailJob;
import ftn.udd.projekt.email.service.EmailService;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.User;

@Service
public class EmailServiceImpl implements EmailService {
	private EmailWorker worker;
	
	@PostConstruct
	private void init() {
		this.worker = new EmailWorker();
		this.worker.init();
	}
	
	@Override
	public void sendEmail(User user, Book book) {
		worker.addJob(new SendMailJob(user, book));
	}

}
