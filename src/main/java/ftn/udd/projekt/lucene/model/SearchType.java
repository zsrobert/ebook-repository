package ftn.udd.projekt.lucene.model;


public enum SearchType {
	REGULAR("Regular"), FUZZY("Fuzzy"), PHRASE("Phrase"), RANGE("Range"), PREFIX("Prefix");

	private String message;

	private SearchType(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
