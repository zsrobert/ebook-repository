package ftn.udd.projekt.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ftn.udd.projekt.model.User;
import ftn.udd.projekt.service.def.UserService;
import ftn.udd.projekt.service.impl.inMemory.InMemoryUserService;

public class InMemoryUserServiceTest {
	
	private UserService userService;
	
	@Before
	public void setUp() {
		this.userService = new InMemoryUserService();
		User pera = new User();
		pera.setEmail("pera@mail.com");
		pera.setFirstName("Pera");
		pera.setLastName("Peric");
		pera.setPassword("sifra");
		
		User mika = new User();
		mika.setFirstName("Mika");
		
		userService.save(pera);
		userService.save(mika);
	}
	
	@Test
	public void testfindOne() {
		User user = userService.findOne(1);
		Assert.assertNotNull(user);
		Assert.assertEquals("Pera", user.getFirstName());
		Assert.assertEquals("Peric", user.getLastName());
		Assert.assertEquals("pera@mail.com", user.getEmail());
		Assert.assertEquals("sifra", user.getPassword());
	}

	@Test
	public void testFindAll() {
		List<User> users = userService.findAll();
		
		Assert.assertEquals(2, users.size());
		
		User user1 = users.get(0);
		User user2 = users.get(1);
		
		if (user1.getId().equals(1)) {
			Assert.assertEquals("Pera", user1.getFirstName());
			
			Assert.assertTrue(user2.getId().equals(2) && "Mika".equals(user2.getFirstName()));
		} else {
			Assert.assertEquals("Mika", user1.getFirstName());
			
			Assert.assertTrue(user2.getId().equals(1) && "Pera".equals(user2.getFirstName()));
		}
	}

	@Test
	public void testSave() {
		User user = new User();
		user.setFirstName("Novi");
		
		user = userService.save(user);
		
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getId());
		
		Assert.assertEquals("Novi", user.getFirstName());
	}

	@Test
	public void testDelete() {
		Assert.assertNotNull(userService.findOne(1));
		Assert.assertNotNull(userService.findOne(2));
		
		userService.delete(1);
		
		Assert.assertNull(userService.findOne(1));
		Assert.assertNotNull(userService.findOne(2));
	}

}
