UDD.Category = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/categories/",

	defaults : {
		id : undefined,
		name : "Unknown"
	},

	validate : function(attrs) {
		if (!attrs.name || attrs.name.trim() == "") {
			return Spring.messages["error.input.category.name"];
		}
	}
});

UDD.User = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/users/",

	defaults : {
		id : undefined,
		email : "Unknown",
		firstName : "Unknown",
		admin : false,
		lastName : "Unknown",
		password : "Unknown"

	},

	validate : function(attrs) {
		if (!attrs.firstName || attrs.firstName.trim() == "") {
			return Spring.messages["error.input.user.firstName"];
		}
		if (!attrs.lastName || attrs.lastName.trim() == "") {
			return Spring.messages["error.input.user.lastName"];
		}
		if (!attrs.email || attrs.email.trim() == "") {
			return Spring.messages["error.input.user.email"];
		}
		if (!attrs.password || attrs.password.trim() == "") {
			return Spring.messages["error.input.user.password"];
		} else {
			if (attrs.password.length < 5) {
				return Spring.messages["error.input.user.password.length"];
			}
		}
	}
});

UDD.Book = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/books/",

	defaults : {
		id : undefined,
		title : undefined,
		description : undefined,
		language : "Unknown",
		publishingDate : undefined,
		category : undefined,
		authors : "Unknown",
		keyWords : undefined
	},

	validate : function(attrs) {
		if (!attrs.title || attrs.title.trim() == "") {
			return Spring.messages["error.input.book.title"];
		}
		if (!attrs.description || attrs.description.trim() == "") {
			return Spring.messages["error.input.book.description"];
		}
		if (!attrs.language || attrs.language.trim() == "") {
			return Spring.messages["error.input.book.language"];
		}
		if (!attrs.category || !attrs.category.id) {
			return Spring.messages["error.input.book.category"];
		}
		if (!attrs.authors || attrs.authors.trim() == "") {
			return Spring.messages["error.input.book.authors"];
		}
		if (!attrs.publishingDate) {
			return Spring.messages["error.input.book.publishingDate"];
		} else if (!isValidDate(attrs.publishingDate)) {
			return Spring.messages["error.input.book.publishingDate.format"]
		}
	}
});

UDD.Author = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/authors/",

	defaults : {
		id : undefined,
		name : "Unknown"
	},

	validate : function(attrs) {
		if (!attrs.name || attrs.name.trim() == "") {
			return Spring.messages["error.input.author.name"];
		}
	}
});

UDD.CategoryField = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/categories/fields"
});

UDD.UserField = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/users/fields"
});

UDD.AuthorField = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/authors/fields"
});

UDD.BookField = Backbone.Model.extend({
	urlRoot : appConfig.appRootPath + "/books/fields"
});

UDD.Categories = Backbone.Collection.extend({
	model : UDD.Category,
	url : appConfig.appRootPath + "/categories/"
});

UDD.Users = Backbone.Collection.extend({
	model : UDD.User,
	url : appConfig.appRootPath + "/users/"
});

UDD.Authors = Backbone.Collection.extend({
	model : UDD.Author,
	url : appConfig.appRootPath + "/authors/"
});

UDD.Books = Backbone.Collection.extend({
	model : UDD.Book,
	url : appConfig.appRootPath + "/books/"
});

UDD.CategoryFields = Backbone.Collection.extend({
	model : UDD.CategoryField,
	url : appConfig.appRootPath + "/categories/fields"
});

UDD.UserFields = Backbone.Collection.extend({
	model : UDD.UserField,
	url : appConfig.appRootPath + "/users/fields"
});

UDD.AuthorFields = Backbone.Collection.extend({
	model : UDD.UserField,
	url : appConfig.appRootPath + "/authors/fields"
});

UDD.BookFields = Backbone.Collection.extend({
	model : UDD.UserField,
	url : appConfig.appRootPath + "/books/fields"
});

function isValidDate(dateString) {
	// First check for the pattern
	if (!/^\d{1,2}\.\d{1,2}\.\d{4}$/.test(dateString))
		return false;

	// Parse the date parts to integers
	var parts = dateString.split(".");
	var day = parseInt(parts[0], 10);
	var month = parseInt(parts[1], 10);
	var year = parseInt(parts[2], 10);

	// Check the ranges of month and year
	if (year < 1000 || year > 3000 || month == 0 || month > 12)
		return false;

	var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

	// Adjust for leap years
	if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
		monthLength[1] = 29;

	// Check the range of the day
	return day > 0 && day <= monthLength[month - 1];
}