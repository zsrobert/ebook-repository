package ftn.udd.projekt.email;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailWorker {
	private static final Logger logger = LoggerFactory
			.getLogger(EmailWorker.class);
	public static final String EMAIL = "udduddic@gmail.com";
	public static final String PASS = "udduddic2013";
	public static final Properties PROPS = new Properties();
	static {
		PROPS.put("mail.smtp.auth", "true");
		PROPS.put("mail.smtp.starttls.enable", "true");
		PROPS.put("mail.smtp.host", "smtp.gmail.com");
		PROPS.put("mail.smtp.port", "587");
	}

	private BlockingQueue<Runnable> jobs = new ArrayBlockingQueue<>(100, true);
	private Thread worker;
	private boolean working = true;

	@PostConstruct
	public void init() {
		worker = new Thread(new Runnable() {
			@Override
			public void run() {
				while (working) {
					Runnable job = jobs.poll();

					if (job != null) {
						job.run();
						logger.info("Email sent.");
					}
				}
			}
		});
		worker.start();
	}

	public void stop() {
		this.working = false;
	}

	public void addJob(Runnable job) {
		jobs.add(job);
	}
}
