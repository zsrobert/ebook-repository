package ftn.udd.projekt.integration.model;


public class UserIntegration {

	/**
	 * Session id.
	 */
	private String userId;
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public UserIntegration(String userId) {
		this.userId = userId;
	}

	public UserIntegration() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserIntegration other = (UserIntegration) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
