package ftn.udd.projekt.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.Category;

public class BookStoreDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 4022076473192702430L;

	@NotBlank
	private String title;

	@NotBlank
	private List<Author> authors;

	@NotBlank
	private String description;

	/**
	 * Comma separated list of keywords.
	 */
	private String keyWords;

	@NotBlank
	private String language;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
	private Date publishingDate;

	private Category category;

	public BookStoreDTO() {
		super();
	}
	
	public BookStoreDTO(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.description = book.getDescription();
		this.language = book.getLanguage();
		this.category = book.getCategory();
		this.publishingDate = book.getPublishingDate();
		this.authors = new ArrayList<Author>(book.getAuthors());
		
		this.keyWords = StringUtils.join(book.getKeyWords(), ", ");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Date getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(Date publishingDate) {
		this.publishingDate = publishingDate;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	

}
