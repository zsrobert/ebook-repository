package ftn.udd.projekt.json.deserializer;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import ftn.udd.projekt.dto.CategoryDTO;
import ftn.udd.projekt.dto.UserDTO;
import ftn.udd.projekt.service.def.CategoryService;

public class UserDeserializer extends JsonDeserializer<UserDTO> {

	@Autowired
	private CategoryService categoryService;

	public UserDeserializer() {
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
	}

	@Override
	public UserDTO deserialize(JsonParser jp, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		UserDTO user = new UserDTO();

		if (node.has("id")) {
			user.setId(node.get("id").asInt());
		}
		String firstName = node.get("firstName").asText();
		String lastName = node.get("lastName").asText();
		String email = node.get("email").asText();
		String password = node.get("password").asText();

		if (node.has("id") && node.has("subscribedTo")) {
			int catId = node.get("subscribedTo").asInt();
			CategoryDTO subscribedTo = new CategoryDTO();
			subscribedTo.setId(catId);
			
			user.setSubscribedTo(subscribedTo);
		}
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(password);

		return user;
	}

}
