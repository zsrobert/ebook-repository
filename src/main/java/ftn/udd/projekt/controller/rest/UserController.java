package ftn.udd.projekt.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.udd.projekt.dto.CategoryDTO;
import ftn.udd.projekt.dto.FieldDTO;
import ftn.udd.projekt.dto.UserDTO;
import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.model.Subscription;
import ftn.udd.projekt.model.User;
import ftn.udd.projekt.service.def.CategoryService;
import ftn.udd.projekt.service.def.UserService;
import ftn.udd.projekt.util.ClassFieldUtils;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private AuthenticationManager authenticationManager;

	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);
	
	@PostConstruct
	private void init() {
		User user = new User();
		user.setAdmin(true);
		user.setEmail("admin@admin.com");
		user.setFirstName("Admin");
		user.setLastName("Adminic");
		user.setPassword("admin");
		
		try {
			userService.save(user);
		} catch (Exception e) {
			// chill
		}
	}
	
	@RequestMapping(value = "/rest/users", method = RequestMethod.GET)
	public List<UserDTO> handleGetUsers() {
		List<User> users = userService.findAll();
		List<UserDTO> response = new ArrayList<UserDTO>();

		for (User user : users) {
			UserDTO u = new UserDTO(user);
			u.setPassword("-");
			response.add(u);
		}

		return response;
	}

	@RequestMapping(value = "/rest/users/{id}", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> handleGetUser(@PathVariable Integer id) {
		User user = userService.findOne(id);
		ResponseEntity<UserDTO> response;

		if (user != null) {
			response = new ResponseEntity<UserDTO>(new UserDTO(user),
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/auth/user", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> handleGetAuthUser() {
		ResponseEntity<UserDTO> response = new ResponseEntity<UserDTO>(
				HttpStatus.FORBIDDEN);

		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();

			if (userDetails != null) {
				User user = userService.findByEmail(userDetails.getUsername());

				if (user != null) {
					response = new ResponseEntity<>(new UserDTO(user),
							HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			// do nothing
		}

		return response;
	}

	@RequestMapping(value = "/rest/users/subscribe/{id}")
	public ResponseEntity<CategoryDTO> handleSubscribe(@PathVariable Integer id) {
		ResponseEntity<CategoryDTO> response = new ResponseEntity<CategoryDTO>(
				HttpStatus.FORBIDDEN);

		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();

			if (userDetails != null) {
				User user = userService.findByEmail(userDetails.getUsername());

				if (user != null) {
					Category category = categoryService.findOne(id);

					if (category != null || id == 0) {
						Subscription subscription = new Subscription();
						if (id != 0) {
							subscription.setCategory(category);
						} else {
							category = new Category();
							category.setId(0);
							category.setName("All");
						}
						subscription.setUser(user);
						user.setSubscription(subscription);
						userService.save(user);
						response = new ResponseEntity<>(new CategoryDTO(
								category), HttpStatus.OK);
					}
				}
			}
		} catch (Exception e) {
			// do nothing
			e.printStackTrace();
		}

		return response;
	}

	@RequestMapping(value = "/rest/users/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<UserDTO> handleDeleteUser(@PathVariable Integer id) {
		User user = userService.findOne(id);
		ResponseEntity<UserDTO> response;

		if (user != null) {
			userService.delete(id);
			response = new ResponseEntity<>(new UserDTO(user), HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/rest/users", method = RequestMethod.POST)
	public ResponseEntity<UserDTO> handlePostUser(
			@RequestBody @Valid UserDTO userDTO) {
		User user = new User(userDTO);
		ResponseEntity<UserDTO> response;

		user = userService.save(user);

		if (user != null && user.getId() != null) {
			UserDetails userDetails = userDetailsService
					.loadUserByUsername(user.getEmail());
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					userDetails, user.getPassword(),
					userDetails.getAuthorities());
			authenticationManager.authenticate(token);

			if (token.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(token);
			}

			logger.info("User #" + user.getId() + " registered.");
			response = new ResponseEntity<UserDTO>(new UserDTO(user),
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}

	@RequestMapping(value = "/rest/users/{id}", method = RequestMethod.PUT)
	public ResponseEntity<UserDTO> handlePutUser(
			@RequestBody @Valid UserDTO userDTO, BindingResult bindingResult,
			@PathVariable Integer id) {
		User user = userService.findOne(id);
		userDTO.setId(id);
		userDTO.setAdmin(user.isAdmin());

		user = new User(userDTO);

		if (user.getSubscription() != null
				&& user.getSubscription().getCategory() != null) {
			Category cat = categoryService.findOne(user.getSubscription().getCategory().getId());
			user.getSubscription().setCategory(cat);
		}

		user = userService.save(user);

		ResponseEntity<UserDTO> response = new ResponseEntity<UserDTO>(
				new UserDTO(user), HttpStatus.OK);

		return response;
	}

	@RequestMapping(value = "/rest/users/fields", method = RequestMethod.GET)
	public List<FieldDTO> handleFieldsGet() {
		return ClassFieldUtils.getFieldDTOs(UserDTO.class);
	}

}
