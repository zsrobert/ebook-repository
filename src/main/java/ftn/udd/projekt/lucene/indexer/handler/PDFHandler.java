package ftn.udd.projekt.lucene.indexer.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

import ftn.udd.projekt.lucene.field.FieldWithVectors;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.PDFBook;
import ftn.udd.projekt.util.pdfparser.PDFParser;

public class PDFHandler implements DocumentHandler {

	public Document getDocument(PDFBook book, Book bookModel) {
		Document doc = new Document();
		PDFParser parser;
		try {
			parser = new PDFParser(book.getFileBytes());
			parser.parse();
			// grab the PDF's content
			PDDocument pdf = parser.getPDDocument();
			PDFTextStripper textStripper = new PDFTextStripper("utf-8");
			String text = "" + textStripper.getText(pdf);
			doc.add(new FieldWithVectors("text", text, Store.NO));
			List<IndexableField> fields = bookModel.getIndexableFields();
			for (IndexableField field : fields) {
				doc.add(field);
			}
			pdf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return doc;
	}
	
	public Document getDocument(File file) {
		Document doc = new Document();

		try {
			PDFParser parser = new PDFParser(new FileInputStream(file));
			parser.parse();

			// grab the PDF's content
			PDDocument pdf = parser.getPDDocument();
			PDFTextStripper textStripper = new PDFTextStripper("utf-8");
			String text = "" + textStripper.getText(pdf);
			doc.add(new TextField("text", text, Store.NO));

			pdf.close();
		} catch (IOException e) {
			System.out.print("" + e.getMessage());
		}

		return doc;
	}
}
