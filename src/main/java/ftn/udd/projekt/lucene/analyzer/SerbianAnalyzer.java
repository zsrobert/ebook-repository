package ftn.udd.projekt.lucene.analyzer;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

import ftn.udd.projekt.lucene.analyzer.filter.CyrilicToLatinFilter;
import ftn.udd.projekt.lucene.analyzer.stemmer.SimpleSerbianStemmer;


/**
 * Analyser for Lucene, it is latin - cyrilic insensitive
 * 
 * @author chenejac@uns.ac.rs
 * 
 */
public class SerbianAnalyzer extends Analyzer {

	Version version;
	
	private static final String[] SERBIAN_STOP_WORDS = {
		"biti", "ne", 
		"jesam", "sam", "jesi", "si", "je", "jesmo", "smo", "jeste", "ste", "jesu", "su",
		"nijesam", "nisam", "nijesi", "nisi", "nije", "nijesmo", "nismo", "nijeste", "niste", "nijesu", "nisu",
		"budem", "budeš", "bude", "budemo", "budete", "budu",
		"budes",
		"bih",  "bi", "bismo", "biste", "biše", 
		"bise",
		"bio", "bili", "budimo", "budite", "bila", "bilo", "bile", 
		"ću", "ćeš", "će", "ćemo", "ćete", 
		"neću", "nećeš", "neće", "nećemo", "nećete", 
		"cu", "ces", "ce", "cemo", "cete",
		"necu", "neces", "nece", "necemo", "necete",
		"mogu", "možeš", "može", "možemo", "možete",
		"mozes", "moze", "mozemo", "mozete"};
	
	/**
	 * 
	 */
	public SerbianAnalyzer(Version version) {
		super();
		this.version = version;
	}


	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
		Tokenizer source = new StandardTokenizer(version, reader);
		
		TokenStream result = new LowerCaseFilter(version, source);
		result = new CyrilicToLatinFilter(result);
		result = new StopFilter(version, result, StopFilter.makeStopSet(version, SERBIAN_STOP_WORDS));
		result = new SnowballFilter(result, new SimpleSerbianStemmer());
		result = new ASCIIFoldingFilter(result);
		
		return new TokenStreamComponents(source, result);
	}

}
