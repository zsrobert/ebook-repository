package ftn.udd.projekt.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ftn.udd.projekt.json.deserializer.UserDeserializer;
import ftn.udd.projekt.model.User;

@JsonDeserialize(using = UserDeserializer.class)
public class UserDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 5081981364368881631L;

	@NotBlank
	private String firstName;

	@NotBlank
	private String lastName;

	@Email
	private String email;

	@Length(min = 5)
	@NotBlank
	private String password;
	
	private boolean isAdmin;

	private CategoryDTO subscribedTo;

	public UserDTO() {
		super();
	}

	public UserDTO(User user) {
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		
		if (user.getSubscription() != null) {
			if (user.getSubscription().getCategory() == null) {
				this.subscribedTo = new CategoryDTO();
				this.subscribedTo.setId(0);
			} else {
				this.subscribedTo = new CategoryDTO(user.getSubscription().getCategory());
			}
		} else {
			this.subscribedTo = new CategoryDTO();
			this.subscribedTo.setId(-1);
		}
		
		this.password = user.getPassword();
		this.isAdmin = user.isAdmin();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CategoryDTO getSubscribedTo() {
		return subscribedTo;
	}

	public void setSubscribedTo(CategoryDTO subscribedTo) {
		this.subscribedTo = subscribedTo;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

}
