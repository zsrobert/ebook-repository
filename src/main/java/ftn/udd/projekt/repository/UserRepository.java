package ftn.udd.projekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	public User findByEmail(String email);
	
	@Query("SELECT u FROM User u WHERE u.subscription.category = :category OR u.subscription.category is null")
	public List<User> findAllWithSubscription(@Param("category") Category category);
}
