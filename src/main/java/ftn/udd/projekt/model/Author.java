package ftn.udd.projekt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import ftn.udd.projekt.dto.AuthorDTO;

@Entity
@Table(name = "author_info")
public class Author extends BaseModel implements Serializable {
	
	private static final long serialVersionUID = -2124531968092721343L;
	
	@Column(nullable = false, length = 255)
	@NotBlank
	private String name;
	
	public Author() {
		super();
	}
	
	public Author(AuthorDTO authorDTO) {
		this.name = authorDTO.getName();
		this.id = authorDTO.getId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
