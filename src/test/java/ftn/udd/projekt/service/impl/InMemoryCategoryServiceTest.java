package ftn.udd.projekt.service.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.service.def.CategoryService;
import ftn.udd.projekt.service.impl.inMemory.InMemoryCategoryService;

public class InMemoryCategoryServiceTest {

private CategoryService categoryService;
	
	@Before
	public void setUp() {
		this.categoryService = new InMemoryCategoryService();
		Category roman = new Category();
		roman.setName("Roman");
		
		Category triler = new Category();
		triler.setName("Triler");
		
		categoryService.save(roman);
		categoryService.save(triler);
	}
	
	@Test
	public void testfindOne() {
		Category category = categoryService.findOne(1);
		Assert.assertNotNull(category);
		Assert.assertEquals("Roman", category.getName());
		Assert.assertNull(categoryService.findOne(3));
	}

	@Test
	public void testFindAll() {
		List<Category> categories = categoryService.findAll();
		
		Assert.assertEquals(2, categories.size());
		
		Category category1 = categories.get(0);
		Category category2 = categories.get(1);
		
		if (category1.getId().equals(1)) {
			Assert.assertEquals("Roman", category1.getName());
			
			Assert.assertTrue(category2.getId().equals(2) && "Triler".equals(category2.getName()));
		} else {
			Assert.assertEquals("Triler", category1.getName());
			
			Assert.assertTrue(category2.getId().equals(1) && "Roman".equals(category2.getName()));
		}
	}

	@Test
	public void testSave() {
		Category category = new Category();
		category.setName("Nova");
		
		category = categoryService.save(category);
		
		Assert.assertNotNull(category);
		Assert.assertNotNull(category.getId());
		
		Assert.assertEquals("Nova", category.getName());
	}

	@Test
	public void testDelete() {
		Assert.assertNotNull(categoryService.findOne(1));
		Assert.assertNotNull(categoryService.findOne(2));
		
		categoryService.delete(1);
		
		Assert.assertNull(categoryService.findOne(1));
		Assert.assertNotNull(categoryService.findOne(2));
	}

}
