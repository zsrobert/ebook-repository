var Base64 = {
  encode: function(s) {
    return btoa(encodeURIComponent(s));
  },
  decode: function(s) {
    return decodeURIComponent(atob(s));
  }
};

var appConfig = {
	appRootPath : "http://localhost:8080/UddProjekt/rest",
	suggestionsRootPath : "http://localhost:8080/UddProjekt_suggestions",
	downloadPath : "http://localhost:8080/UddProjekt/books/download/"
};

var UDD = window.UDD || {};

getAuthUser();

UDD.View = Backbone.View.extend({
	unset : function() {
		this.undelegateEvents();
		this.$el.removeData().unbind();
		this.$el.empty();
		this.stopListening();
	}
});

UDD.ViewsMap = [];

UDD.ViewManager = {
	showView : function(view) {
		
		var oldView = UDD.ViewsMap[view.$el.attr("id")];
		
		if (oldView && oldView instanceof UDD.View) {
			oldView.unset();
		}
		
		UDD.ViewsMap[view.$el.attr("id")] = view;
		view.render();
		view.delegateEvents();
	}	
};

UDD.AppRouter = Backbone.Router.extend({
	routes : {
		"" : "index",
		"category/:id" : "category",
		"book/:id" : "book",
		"author/:id" : "author",
		"search/:query" : "search",
		"advanced" : "advanced",
		"similar/:id" : "similar",
		"user" : "user"
	},
	index : function() {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();

		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model,
				selected : -1
			});
			UDD.ViewManager.showView(menuView);
		});
		var books = new UDD.Books();
		books.fetch({
			success : function(model, response, options) {
				var categoryBooksView = new UDD.CategoryBooksView({
					items : books,
					title : Spring.messages["text.bookstore.allbooks"]
				});
				UDD.ViewManager.showView(categoryBooksView);
				setupSwitchView();
			},
			error : function(model, response, options) {
				console.log("Error fetching books. Response: ");
				console.log(response);
			}
		});

		// var users = new UDD.Users();
		// var registerModalView = new UDD.RegisterModalView({collection:
		// users});
		// registerModalView.render();
		//
		// var indexView = new UDD.IndexView();
		// indexView.render();
	},
	category : function(id) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();
		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model,
				selected : id
			});
			UDD.ViewManager.showView(menuView);
		});

		var books = new UDD.CategoryBooks({
			id : id
		});
		books.fetch({
			success : function(model, response, options) {
				getCategories(function(categories) {
					var category = categories.get(id);
					var title = "Unknown";
					if (category) {
						title = category.attributes.name;
					}
					var categoryBooksView = new UDD.CategoryBooksView({
						items : model,
						title : title
					});
					UDD.ViewManager.showView(categoryBooksView);
					setupSwitchView();
				});
			},
			error : function(model, response, options) {
				console.log("Error fetching books in category. Response: ");
				console.log(response);
			}
		});
	},

	author : function(id) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();
		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model,
			});
			UDD.ViewManager.showView(menuView);
		});

		var books = new UDD.AuthorBooks({
			id : id
		});

		books.fetch({
			success : function(model, response, options) {
				var author = new UDD.Author({
					id : id
				});
				author.fetch({
					success : function(data) {
						var title = data.attributes.name;
						var categoryBooksView = new UDD.CategoryBooksView({
							items : model,
							title : title
						});
						UDD.ViewManager.showView(categoryBooksView);
						setupSwitchView();
					},
					error : function(response) {
						console.log("Error fetching author. Response: ");
						console.log(response);
					}
				});
			},
			error : function(model, response, options) {
				console.log("Error fetching books by author. Response: ");
				console.log(response);
			}
		});
	},

	book : function(id) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();

		var book = new UDD.Book({
			id : id
		});

		book.fetch({
			success : function(data) {
				var bookInfoView = new UDD.BookInfoView({
					item : data.attributes
				});
				UDD.ViewManager.showView(bookInfoView);

				var bookContentView = new UDD.BookContentView({
					item : data.attributes
				});
				UDD.ViewManager.showView(bookContentView);
			},
			error : function(response) {
				console.log("Error fetching book. Response: ");
				console.log(response);
			}
		});
	},

	search : function(query) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();

		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model
			});
			UDD.ViewManager.showView(menuView);
		});
		console.log(Base64.decode(query));
		var results = new UDD.SearchResults({
			query : query
		});

		results.fetch({
			success : function(model) {
				var view = new UDD.SearchResultsView({
					items : model,
					didYouMean : model.didYouMean
				});
				UDD.ViewManager.showView(view);
			},

			error : function(model, response) {
				console.log("Error fetching books. Response: ");
				console.log(response);
			}
		});
	},

	advanced : function() {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();

		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model
			});
			UDD.ViewManager.showView(menuView);
		});

		var content = new UDD.AdvancedSearchView();
		UDD.ViewManager.showView(content);
	},

	similar : function(id) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();

		getCategories(function(model) {
			var menuView = new UDD.LeftMenuView({
				items : model,
			});
			UDD.ViewManager.showView(menuView);
		});

		var books = new UDD.SimilarBooks({
			id : id
		});

		books.fetch({
			success : function(model, response, options) {
				var book = new UDD.Book({
					id : id
				});
				book.fetch({
					success : function(data) {
						var title = "Books similar to: "
								+ data.attributes.title;
						var categoryBooksView = new UDD.CategoryBooksView({
							items : model,
							title : title
						});
						UDD.ViewManager.showView(categoryBooksView);
						setupSwitchView();
					},
					error : function(response) {
						console.log("Error fetching book. Response: ");
						console.log(response);
					}
				});
			},
			error : function(model, response, options) {
				console.log("Error fetching similar books. Response: ");
				console.log(response);
			}
		});
	},
	
	user : function(retry) {
		var headerView = new UDD.HeaderView();
		UDD.ViewManager.showView(headerView);
		setupHeader();
		
		if (UDD.user && UDD.user.attributes && UDD.user.attributes.id) {
			getCategories(function(model) {
				var view = new UDD.UserView({
					categories : model,
					user : UDD.user.toJSON()
				});
				
				var menuView = new UDD.LeftMenuView({
					items : model
				});
				UDD.ViewManager.showView(menuView);
				
				UDD.ViewManager.showView(view);
			});
		} else {
			if (retry === false) {
				alert("Not ALAUD!");
			} else {
				var self = this;
				setTimeout(function() { self.user(false) }, 1000);
			}
			
			
		}
	}
});

function setupHeader() {
	var originalWidth = $(".input-enlarge").width();
	$(".input-enlarge").focus(function() {
		$(this).animate({
			width : "300px"
		}, 300);
	}).blur(function() {
		$(this).animate({
			width : originalWidth + "px"
		}, 300);
	});
}

function setupSwitchView() {
	$("#view-list").click(function() {
		$(".mix-container").mixItUp("changeLayout", {
			containerClass : "list"
		});
	});

	$("#view-grid").click(function() {
		$(".mix-container").mixItUp("changeLayout", {
			containerClass : "grid"
		});
	});

	$(".mix-container").mixItUp({
		animation : {
			animateChangeLayout : true,
			animateResizeTargets : true,
			effects : "fade rotateX(-40deg) translateZ(-100px)"
		},
		layout : {
			containerClass : "grid"
		}
	});
}

function getAuthUser() {
	UDD.user = {};
	$.ajax({
		url : "auth/user",
		type : "GET",
		dataType : "json",
		success : function(data) {
			$(".loggedInCheck").hide();
			if (data.id) {
				UDD.user = new UDD.User(data);
				$(".loggedOutItem").addClass("hidden");
				$(".loggedInItem").removeClass("hidden");
				
				if (UDD.user.attributes.admin) {
					$(".adminItem").removeClass("hidden");
				}
			} else {
				$(".loggedInItem").addClass("hidden");
				$(".loggedOutItem").removeClass("hidden");
			}
		},
		error : function(resp) {
			$(".loggedInCheck").addClass("hidden");
			$(".loggedOutItem").removeClass("hidden");
		}
	});
}

function getCategories(callback) {
	if (UDD.categories) {
		callback(UDD.categories);
	} else {
		var categories = new UDD.Categories();
		categories.fetch({
			success : function(model, response, options) {
				UDD.categories = model;
				callback(model);
			},
			error : function(model, response, options) {
				console.log("Error fetching categories. Response: ");
				console.log(response);
			}
		});
	}
}

Handlebars.registerHelper('ifCond', function(v1, operator, v2, options) {

	switch (operator) {
	case '==':
		return (v1 == v2) ? options.fn(this) : options.inverse(this);
	case '===':
		return (v1 === v2) ? options.fn(this) : options.inverse(this);
	case '<':
		return (v1 < v2) ? options.fn(this) : options.inverse(this);
	case '<=':
		return (v1 <= v2) ? options.fn(this) : options.inverse(this);
	case '>':
		return (v1 > v2) ? options.fn(this) : options.inverse(this);
	case '>=':
		return (v1 >= v2) ? options.fn(this) : options.inverse(this);
	case '&&':
		return (v1 && v2) ? options.fn(this) : options.inverse(this);
	case '||':
		return (v1 || v2) ? options.fn(this) : options.inverse(this);
	default:
		return options.inverse(this);
	}
});
