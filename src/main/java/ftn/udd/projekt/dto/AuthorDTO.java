package ftn.udd.projekt.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import ftn.udd.projekt.model.Author;

public class AuthorDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 828520900044416528L;
	
	@NotBlank
	private String name;
	
	public AuthorDTO() {
		super();
	}
	
	public AuthorDTO(Author author) {
		this.id = author.getId();
		this.name = author.getName();
	}

	public String getName() {
		return name;
	}

	public void setLastName(String name) {
		this.name = name;
	}
	
	

}
