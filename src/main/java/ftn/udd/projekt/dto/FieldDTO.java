package ftn.udd.projekt.dto;

import java.io.Serializable;

public class FieldDTO implements Serializable, Comparable<FieldDTO> {

	private static final long serialVersionUID = -3942156865528587247L;

	public FieldDTO(String name) {
		super();
		this.name = name;
	}

	public FieldDTO() {
		super();
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(FieldDTO o) {
		if (o == null)
			return 1;
		return this.name.compareTo(o.name);
	}
}
