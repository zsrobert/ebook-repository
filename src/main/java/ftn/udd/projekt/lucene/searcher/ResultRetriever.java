package ftn.udd.projekt.lucene.searcher;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Bits;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ftn.udd.projekt.dto.BookStoreDTO;
import ftn.udd.projekt.dto.SearchDTO;
import ftn.udd.projekt.lucene.analyzer.SerbianAnalyzer;
import ftn.udd.projekt.lucene.indexer.handler.DocumentHandler;
import ftn.udd.projekt.lucene.indexer.handler.PDFHandler;
import ftn.udd.projekt.lucene.model.SearchType;
import ftn.udd.projekt.lucene.query.QueryBuilder;
import ftn.udd.projekt.lucene.similarity.CosineDocumentSimilarity;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.SearchResultBook;
import ftn.udd.projekt.service.def.BookService;

@Component
public class ResultRetriever {

	private static int MAX_HITS = 10;
	private static final Version MATCH_VERSION = Version.LUCENE_48;
	@Value("${location.index}")
	private String indexPath;
	@Value("${location.books}")
	private String booksPath;

	@Autowired
	private BookService bookService;

	public static void setMaxHits(int maxHits) {
		ResultRetriever.MAX_HITS = maxHits;
	}

	public static int getMaxHits() {
		return ResultRetriever.MAX_HITS;
	}

	public List<SearchResultBook> getResults(SearchDTO searchQuery) {
		Query query = null;
		List<SearchResultBook> results = new ArrayList<SearchResultBook>();

		if (searchQuery == null
				|| (query = searchDTOtoQuery(searchQuery)) == null) {
			return results;
		}

		boolean highlightNeeded = searchQuery.getText() != null
				&& !searchQuery.getText().isEmpty();

		try {
			Directory indexDir = new SimpleFSDirectory(new File(indexPath));
			DirectoryReader reader = DirectoryReader.open(indexDir);
			IndexSearcher is = new IndexSearcher(reader);
			TopScoreDocCollector collector = TopScoreDocCollector.create(
					MAX_HITS, true);

			is.search(query, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			Document doc;
			Highlighter hl;
			SerbianAnalyzer sa = new SerbianAnalyzer(MATCH_VERSION);

			for (ScoreDoc sd : hits) {
				doc = is.doc(sd.doc);

				int id = Integer.parseInt(doc.get("id"));
				Book book = bookService.findOne(id);
				SearchResultBook result = new SearchResultBook(book);
				if (highlightNeeded) {
					DocumentHandler handler = new PDFHandler();
					try {
						hl = new Highlighter(new QueryScorer(query, reader,
								"text"));
						Document pdf = handler.getDocument(new File(booksPath
								+ File.separator + id + ".pdf"));
						String text = hl.getBestFragment(sa, "text",
								"" + pdf.get("text"));
						if (text != null) {
							String highlight = "... " + text + " ...";
							result.setHighlightedText(highlight);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				results.add(result);
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return results;
	}

	public List<BookStoreDTO> getMoreLikeThis(Integer id) {
		List<BookStoreDTO> results = new ArrayList<>();
		try {
			Directory indexDir = new SimpleFSDirectory(new File(indexPath));
			DirectoryReader reader = DirectoryReader.open(indexDir);
			// MoreLikeThis mlt = new MoreLikeThis(reader);
			// mlt.setFieldNames(new String[] { "text" });
			// mlt.setAnalyzer(new SerbianAnalyzer(MATCH_VERSION));
			IndexSearcher is = new IndexSearcher(reader);

			Term term = new Term("id", "" + id);
			TermQuery query = new TermQuery(term);
			TopScoreDocCollector collector = TopScoreDocCollector.create(1,
					true);

			is.search(query, collector);
			int docNum = collector.topDocs().scoreDocs[0].doc;
			// Query likeQuery = mlt.like(docNum);
			// TopDocs topDocs = is.search(likeQuery, MAX_HITS);

			Bits liveDocs = MultiFields.getLiveDocs(reader);
			for (int i = 0; i < reader.maxDoc(); i++) {
				if (liveDocs != null && !liveDocs.get(i))
					continue;
				Document doc = is.doc(i);

				double similarity = CosineDocumentSimilarity
						.getCosineSimilarity(docNum, i, indexPath);

				System.out.println("Document \"" + doc.get("title") + "\" has "
						+ (new DecimalFormat("#0.00")).format(similarity)
						+ " similarity.");

				if (similarity > 0.7) {
					int docId = Integer.parseInt(doc.get("id"));
					Book book = bookService.findOne(docId);
					results.add(new BookStoreDTO(book));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return results;
	}

	private Query searchDTOtoQuery(SearchDTO searchQuery) {
		BooleanQuery query = new BooleanQuery();
		SearchType queryType = searchQuery.getType();
		if (queryType == null) {
			queryType = SearchType.REGULAR;
		}
		if (searchQuery.getAuthors() != null
				&& !searchQuery.getAuthors().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "author",
							searchQuery.getAuthors()),
					searchQuery.getAuthorsOperator());
			;
		}
		if (searchQuery.getCategory() != null
				&& !searchQuery.getCategory().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "category",
							searchQuery.getCategory()),
					searchQuery.getCategoryOperator());
			;
		}
		if (searchQuery.getKeywords() != null
				&& !searchQuery.getKeywords().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "keyword",
							searchQuery.getKeywords()),
					searchQuery.getKeywordsOperator());
			;
		}
		if (searchQuery.getLanguage() != null
				&& !searchQuery.getLanguage().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "language",
							searchQuery.getLanguage()),
					searchQuery.getLanguageOperator());
			;
		}
		if (searchQuery.getText() != null && !searchQuery.getText().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "text",
							searchQuery.getText()),
					searchQuery.getTextOperator());
			;
		}
		if (searchQuery.getTitle() != null && !searchQuery.getTitle().isEmpty()) {
			query.add(
					QueryBuilder.buildQuery(queryType, "title",
							searchQuery.getTitle()),
					searchQuery.getTitleOperator());
		}
		return query;
	}
}
