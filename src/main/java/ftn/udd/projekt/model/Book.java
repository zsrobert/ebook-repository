package ftn.udd.projekt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;
import org.hibernate.validator.constraints.NotBlank;

import ftn.udd.projekt.dto.BookAdminDTO;

@Entity
@Table(name = "book_info")
public class Book extends BaseModel implements Serializable {

	private static final long serialVersionUID = -702063467437509479L;

	@Column(nullable = false, length = 255)
	@NotBlank
	private String title;

	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	private Set<Author> authors;

	@Column(nullable = true, length = 255)
	private String description;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "book_keywords", joinColumns = @JoinColumn(name = "book_id"))
	private List<String> keyWords;

	@Column(nullable = true, length = 255)
	private String language;

	@Column(nullable = true)
	private Date publishingDate;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id")
	private Category category;

	@Column
	private Boolean indexed = false;

	public Book() {
		super();
	}

	public Book(BookAdminDTO bookDTO) {
		this.id = bookDTO.getId();
		this.title = bookDTO.getTitle();
		this.description = bookDTO.getDescription();
		this.language = bookDTO.getLanguage();
		this.category = bookDTO.getCategory();
		this.publishingDate = bookDTO.getPublishingDate();

		this.authors = new HashSet<Author>();

		String[] tokens = bookDTO.getAuthors().split(", ");

		for (String authorName : tokens) {
			Author author = new Author();
			author.setName(authorName);

			this.authors.add(author);
		}

		this.keyWords = new ArrayList<String>();

		tokens = bookDTO.getKeyWords().split(", ");

		for (String keyword : tokens) {
			this.keyWords.add(keyword);
		}

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<String> keyWords) {
		this.keyWords = keyWords;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Date getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(Date publishingDate) {
		this.publishingDate = publishingDate;
	}

	public void addAuthor(Author author) {
		if (this.authors == null) {
			this.authors = new HashSet<Author>();
		}

		authors.add(author);
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(Boolean indexed) {
		this.indexed = indexed;
	}

	@Transient
	public List<IndexableField> getIndexableFields() {
		ArrayList<IndexableField> fields = new ArrayList<>();

		fields.add(new TextField("title", this.getTitle(), Store.YES));
		fields.add(new TextField("id", "" + this.getId(), Store.YES));

		for (Author author : this.getAuthors()) {
			fields.add(new TextField("author", author.getName(), Store.YES));
		}
		for (String keyword : this.getKeyWords()) {
			fields.add(new TextField("keyword", keyword, Store.YES));
		}
		if (this.getDescription() != null) {
			fields.add(new TextField("description", this.getDescription(),
					Store.NO));
		}
		if (this.getLanguage() != null) {
			fields.add(new TextField("language", this.getLanguage(), Store.YES));
		}
		if (this.getCategory() != null) {
			fields.add(new TextField("category", this.getCategory().getName(), Store.YES));
		}
		return fields;
	}
}
