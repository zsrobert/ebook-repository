package ftn.udd.projekt.service.impl.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.repository.CategoryRepository;
import ftn.udd.projekt.service.def.CategoryService;

@Service
public class DBCategoryService implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public Category findOne(Integer id) {
		return categoryRepository.findOne(id);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category save(Category item) {
		return categoryRepository.save(item);
	}

	@Override
	public void delete(Integer id) {
		categoryRepository.delete(id);
	}

}
