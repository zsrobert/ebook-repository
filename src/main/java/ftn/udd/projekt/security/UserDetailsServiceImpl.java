package ftn.udd.projekt.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.User;
import ftn.udd.projekt.security.roles.UserRoles;
import ftn.udd.projekt.service.def.UserService;

@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {

		User user = userService.findByEmail(email);
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		if (user != null) {
			if (user.isAdmin()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(
						UserRoles.ROLE_ADMIN));
			} else {
				grantedAuthorities.add(new SimpleGrantedAuthority(
						UserRoles.ROLE_USER));
			}
		}
		
		return new org.springframework.security.core.userdetails.User(
				user.getEmail(), user.getPassword(), grantedAuthorities);
	}

}
