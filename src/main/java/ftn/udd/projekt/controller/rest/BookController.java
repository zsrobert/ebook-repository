package ftn.udd.projekt.controller.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ftn.udd.projekt.dto.BookAdminDTO;
import ftn.udd.projekt.dto.BookStoreDTO;
import ftn.udd.projekt.dto.FieldDTO;
import ftn.udd.projekt.email.service.EmailService;
import ftn.udd.projekt.integration.event.BookViewedEvent2;
import ftn.udd.projekt.integration.model.BookIntegration;
import ftn.udd.projekt.integration.model.UserIntegration;
import ftn.udd.projekt.lucene.indexer.Indexer;
import ftn.udd.projekt.lucene.worker.IndexWorker;
import ftn.udd.projekt.lucene.worker.jobs.DeleteIndexJob;
import ftn.udd.projekt.lucene.worker.jobs.IndexAndSaveJob;
import ftn.udd.projekt.lucene.worker.jobs.JobCallback;
import ftn.udd.projekt.lucene.worker.jobs.UpdateIndexJob;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.PDFBook;
import ftn.udd.projekt.model.User;
import ftn.udd.projekt.service.def.BookService;
import ftn.udd.projekt.service.def.PDFBookService;
import ftn.udd.projekt.service.def.UserService;
import ftn.udd.projekt.util.ClassFieldUtils;
import ftn.udd.projekt.util.FileInfoUtils;

@RestController
public class BookController implements ApplicationEventPublisherAware {
	@Autowired
	private BookService bookService;

	@Autowired
	private UserService userService;

	@Autowired
	private PDFBookService pdfBookService;

	@Autowired
	private IndexWorker indexWorker;

	@Autowired
	private Indexer indexer;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private EmailService emailService;

	@Value("${location.books}")
	private String path;

	@Value("${location.images}")
	private String imagesPath;

	@SuppressWarnings("unused")
	private ApplicationEventPublisher applicationEventPublisher;

	private static final Logger logger = LoggerFactory
			.getLogger(BookController.class);

	@PostConstruct
	public void init() {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	@RequestMapping(value = "/rest/books", method = RequestMethod.GET)
	public List<BookAdminDTO> handleGetBook() {
		List<Book> books = bookService.findAll();
		List<BookAdminDTO> response = new ArrayList<BookAdminDTO>();

		for (Book book : books) {
			response.add(new BookAdminDTO(book));
		}

		return response;
	}

	@RequestMapping(value = "/books/upload", method = RequestMethod.POST)
	public ResponseEntity<BookAdminDTO> handleUploadBook(
			@RequestParam("file") MultipartFile file) {
		ResponseEntity<BookAdminDTO> response;

		if (!file.isEmpty()) {
			BookAdminDTO book = FileInfoUtils.parseBookModelFromFile(file);

			if (book != null) {
				Book bookModel = new Book(book);

				if (bookModel.getTitle() == null
						|| bookModel.getTitle().isEmpty()
						|| "null".equals(bookModel.getTitle())) {
					book.setTitle(file.getOriginalFilename());
					bookModel.setTitle(file.getOriginalFilename());
				}

				bookModel = bookService.save(bookModel);
				book.setId(bookModel.getId());
				logger.info("Book #" + bookModel.getId() + " uploaded");
				try {
					PDFBook pdfBook = new PDFBook(bookModel.getId(),
							file.getBytes());
					pdfBookService.save(pdfBook);
					response = new ResponseEntity<>(book, HttpStatus.OK);
				} catch (IOException e) {
					response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
					e.printStackTrace();
				}

			} else {
				response = new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
			}

		} else {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return response;
	}

	@RequestMapping(value = "/books/{id}/image/upload", method = RequestMethod.POST)
	public ResponseEntity<BookAdminDTO> handleUploadImage(
			@RequestParam("imageFile") MultipartFile file,
			@PathVariable Integer id) {
		ResponseEntity<BookAdminDTO> response;

		if (!file.isEmpty()) {
			try {
				saveFile(imagesPath + "/" + id + ".jpg", file.getBytes());
				response = new ResponseEntity<>(HttpStatus.OK);
				logger.info("Image for book #" + id + " uploaded");
			} catch (IOException e) {
				response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
				e.printStackTrace();
			}
		} else {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return response;
	}

	@RequestMapping(value = "/rest/books/{id}", method = RequestMethod.GET)
	public ResponseEntity<BookStoreDTO> handleGetBook(@PathVariable Integer id,
			@CookieValue(value = "JSESSIONID") String sessionId) {
		Book book = bookService.findOne(id);
		ResponseEntity<BookStoreDTO> response;

		if (book != null) {
			response = new ResponseEntity<BookStoreDTO>(new BookStoreDTO(book),
					HttpStatus.OK);

			BookIntegration b = new BookIntegration("" + book.getId());
			b.setTitle(book.getTitle());

			BookViewedEvent2 viewedEvent = new BookViewedEvent2(
					new UserIntegration(sessionId), b);

			// applicationEventPublisher.publishEvent(viewedEvent);

			// retarded way of doing it:
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(new MediaType("application", "json", Charset
					.forName("utf-8")));
			ObjectMapper mapper = new ObjectMapper();
			HttpEntity<String> entity = null;
			try {
				String json = mapper.writeValueAsString(viewedEvent);
				entity = new HttpEntity<String>(json, headers);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			try {
				RestTemplate restTemplate = new RestTemplate();
				restTemplate.postForObject(
						"http://localhost:8080/UddProjekt_suggestions/view",
						entity, String.class);
			} catch (Exception e) {
				// TODO: handle exception
			}

		} else {
			response = new ResponseEntity<BookStoreDTO>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/books/image/{id}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> handleGetBookImage(@PathVariable Integer id,
			Locale locale) {
		String imagePath = imagesPath + File.separator + id + ".jpg";
		File image = new File(imagePath);

		InputStream in;

		if (!image.exists()) {
			if (locale.getLanguage().equals(new Locale("sr").getLanguage())) {
				in = servletContext
						.getResourceAsStream("/images/default_sr.jpg");
			} else {
				in = servletContext.getResourceAsStream("/images/default.jpg");
			}
		} else {
			in = servletContext.getResourceAsStream("/images/" + id + ".jpg");
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.IMAGE_JPEG);

		try {
			byte[] response = IOUtils.toByteArray(in);
			responseHeaders.setContentLength(response.length);
			return new ResponseEntity<byte[]>(response, responseHeaders,
					HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/books/download/{id}")
	public ResponseEntity<byte[]> handleDownloadBook(@PathVariable Integer id,
			HttpServletRequest request) {
		UserDetails userDetails;
		try {
			userDetails = (UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();

			Book book = bookService.findOne(id);
			User user = userService.findByEmail(userDetails.getUsername());

			// user is admin or user has a subscription to either all categories
			// (null) or the book's category
			if (user.isAdmin()
					|| (user.getSubscription() != null && (user
							.getSubscription().getCategory() == null || user
							.getSubscription().getCategory()
							.equals(book.getCategory())))) {

				if (book != null) {
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.setContentType(MediaType
							.parseMediaType("application/pdf; charset=utf-8"));
					responseHeaders.add(
							"Content-Disposition",
							"attachment; filename=\""
									+ MimeUtility.encodeWord(book.getTitle()
											+ ".pdf", "UTF-8", "Q") + "\"");
					try {
						InputStream in = servletContext
								.getResourceAsStream("/books/" + id + ".pdf");
						byte[] response = IOUtils.toByteArray(in);
						responseHeaders.setContentLength(response.length);
						logger.info("User #" + user.getId()
								+ " started downloading book #" + book.getId());
						return new ResponseEntity<byte[]>(response,
								responseHeaders, HttpStatus.OK);
					} catch (IOException e) {
						e.printStackTrace();
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
				} else {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			} else {
				return new ResponseEntity<byte[]>(HttpStatus.FORBIDDEN);
			}
		} catch (Exception ex) {
			return new ResponseEntity<byte[]>(HttpStatus.FORBIDDEN);
		}

	}

	@RequestMapping(value = "/rest/books/categories/{id}", method = RequestMethod.GET)
	public List<BookAdminDTO> handleGetBooksInCategory(@PathVariable Integer id) {
		List<Book> books = bookService.findAllInCategory(id);
		List<BookAdminDTO> response = new ArrayList<BookAdminDTO>();

		if (books != null) {
			for (Book book : books) {
				response.add(new BookAdminDTO(book));
			}
		}

		return response;
	}

	@RequestMapping(value = "/rest/books/authors/{id}", method = RequestMethod.GET)
	public List<BookAdminDTO> handleGetBooksByAuthor(@PathVariable Integer id) {
		List<Book> books = bookService.findAllByAuthor(id);
		List<BookAdminDTO> response = new ArrayList<BookAdminDTO>();

		if (books != null) {
			for (Book book : books) {
				response.add(new BookAdminDTO(book));
			}
		}

		return response;
	}

	@RequestMapping(value = "/rest/books/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BookAdminDTO> handleDeleteBook(
			@PathVariable Integer id) {
		Book book = bookService.findOne(id);
		ResponseEntity<BookAdminDTO> response;

		if (book != null) {
			bookService.delete(id);
			indexWorker.addJob(new DeleteIndexJob(book, indexer));
			response = new ResponseEntity<>(new BookAdminDTO(book),
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/rest/books", method = RequestMethod.POST)
	public ResponseEntity<BookAdminDTO> handlePostBook(
			@RequestBody @Valid BookAdminDTO bookDTO) {
		Book book = new Book(bookDTO);
		ResponseEntity<BookAdminDTO> response;

		book = bookService.save(book);

		if (book != null && book.getId() != null) {
			response = new ResponseEntity<BookAdminDTO>(new BookAdminDTO(book),
					HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}

	@RequestMapping(value = "/rest/books/{id}", method = RequestMethod.PUT)
	public ResponseEntity<BookAdminDTO> handlePutBook(
			@RequestBody @Valid BookAdminDTO bookDTO, @PathVariable Integer id) {
		bookDTO.setId(id);
		final Book book = bookService.save(new Book(bookDTO));

		ResponseEntity<BookAdminDTO> response;

		if (book != null && book.getId() != null) {
			PDFBook pdfBook = pdfBookService.findOne(bookDTO.getId());

			if (pdfBook != null) {
				pdfBook = FileInfoUtils.setPDFInfo(pdfBook, book);
				final PDFBook newPDFBook = pdfBookService.save(pdfBook);
				final String path = this.path + File.separator
						+ pdfBook.getId() + ".pdf";
				logger.info("Indexing book #" + book.getId() + ".");
				indexWorker.addJob(new IndexAndSaveJob(pdfBook, indexer,
						new JobCallback() {

							@Override
							public void success() {
								saveFile(path, newPDFBook.getFileBytes());
								updateBook(newPDFBook);
								deleteBook(newPDFBook);
								sendEmails(book);
								logger.info("Finished indexing book #"
										+ book.getId() + ".");
							}

							@Override
							public void failure() {
								logger.info("Indexing of book #" + book.getId()
										+ " was unsuccessful.");
							}
						}));
			} else {
				indexWorker.addJob(new UpdateIndexJob(book, indexer));
			}

			response = new ResponseEntity<BookAdminDTO>(new BookAdminDTO(book),
					HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}

	protected void sendEmails(Book book) {
		for (User user : userService.findBySubscription(book.getCategory())) {
			emailService.sendEmail(user, book);
		}

	}

	@RequestMapping(value = "/rest/books/fields", method = RequestMethod.GET)
	public List<FieldDTO> handleFieldsGet() {
		return ClassFieldUtils.getFieldDTOs(BookAdminDTO.class);
	}

	private void deleteBook(PDFBook book) {
		pdfBookService.delete(book.getId());
	}

	private void updateBook(PDFBook book) {
		Book bookModel = bookService.findOne(book.getId());
		bookModel.setIndexed(true);
		bookService.save(bookModel);
	}

	private void saveFile(String path, byte[] bytes) {
		File file = new File(path);
		BufferedOutputStream stream;
		try {
			stream = new BufferedOutputStream(new FileOutputStream(file));
			stream.write(bytes);
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setApplicationEventPublisher(
			ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}
