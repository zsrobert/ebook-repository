package ftn.udd.projekt.lucene.worker.jobs;

import ftn.udd.projekt.lucene.indexer.Indexer;
import ftn.udd.projekt.model.PDFBook;

public class IndexAndSaveJob implements Runnable {
	private PDFBook book;
	private Indexer indexer;
	private JobCallback callback;
	
	public IndexAndSaveJob(PDFBook book, Indexer indexer, JobCallback callback) {
		this.book = book;
		this.indexer = indexer;
		this.callback = callback;
	}

	@Override
	public void run() {
		if (indexer.index(book)) {
			callback.success();
		} else {
			callback.failure();
		}
	}
}
