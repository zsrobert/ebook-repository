package ftn.udd.projekt.email.service;

import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.User;

public interface EmailService {
	public void sendEmail(User user, Book book);
}
