package ftn.udd.projekt.service.impl.inMemory;

import java.util.ArrayList;
import java.util.List;

import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.service.def.BookService;

public final class InMemoryBookService extends InMemoryBaseService<Book>
		implements BookService {

	@Override
	public List<Book> findAllInCategory(Integer id) {
		// TODO
		return new ArrayList<Book>();
	}

	@Override
	public List<Book> findAllByAuthor(Integer id) {
		// TODO Auto-generated method stub
		return new ArrayList<Book>();
	}
}
