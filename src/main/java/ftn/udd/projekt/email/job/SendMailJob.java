package ftn.udd.projekt.email.job;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import ftn.udd.projekt.email.EmailWorker;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.User;

public class SendMailJob implements Runnable {
	private User user;
	private Book book;

	public SendMailJob(User user, Book book) {
		super();
		this.user = user;
		this.book = book;
	}

	@Override
	public void run() {
		Session session = Session.getInstance(EmailWorker.PROPS,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(EmailWorker.EMAIL,
								EmailWorker.PASS);
					}
				});

		try {
			String text = "Dear, "
					+ user.getFirstName()
					+ " we've added a new book to "
					+ book.getCategory().getName()
					+ ". Visit <a href='http://localhost:8080/UddProjekt/#book/"
					+ book.getId() + "'>" + book.getTitle()
					+ "</a> to download it.";
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(EmailWorker.EMAIL));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("New book");
			MimeBodyPart bodypart = new MimeBodyPart();

			bodypart.setHeader("Content-Type", "text/plain; charset=\"utf-8\"");
			bodypart.setContent(text, "text/html; charset=utf-8");
			bodypart.setHeader("Content-Transfer-Encoding", "quoted-printable");
			
			Multipart multipart = new MimeMultipart(); 
			multipart.addBodyPart(bodypart);
			message.setContent(multipart);

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
