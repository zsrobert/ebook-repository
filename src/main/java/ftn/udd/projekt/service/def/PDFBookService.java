package ftn.udd.projekt.service.def;

import ftn.udd.projekt.model.PDFBook;

public interface PDFBookService extends BaseService<PDFBook> {

}
