package ftn.udd.projekt.lucene.indexer;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ftn.udd.projekt.lucene.analyzer.SerbianAnalyzer;
import ftn.udd.projekt.lucene.indexer.handler.DocumentHandler;
import ftn.udd.projekt.lucene.indexer.handler.PDFHandler;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.PDFBook;
import ftn.udd.projekt.service.def.BookService;

@Component
public class Indexer {
	private final Version v = Version.LUCENE_48;
	private File indexDirPath;
	private IndexWriter indexWriter;
	private IndexReader indexReader;
	private SpellChecker spellChecker;
	private Directory indexDir;
	private Directory spellcheckerIndexDir;

	@Autowired
	private BookService bookService;
	
	@Value("${location.index}")
	private String path;
	
	@Value("${location.spellchecker.index}")
	private String spellcheckerIndexPath;
	
	@PostConstruct
	public void init() {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
		
		File scFile = new File(spellcheckerIndexPath);
		if (!scFile.exists()) {
			scFile.mkdirs();
		}
		
		IndexWriterConfig iwc = new IndexWriterConfig(v, new SerbianAnalyzer(v));
		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);

		try {
			this.indexDir = new SimpleFSDirectory(file);
			this.spellcheckerIndexDir = new SimpleFSDirectory(scFile);
			this.indexWriter = new IndexWriter(this.indexDir, iwc);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			//throw new IllegalArgumentException("Path not correct");
		}
	}
	
	@PreDestroy
	public void deinit() {
		try {
			this.indexDir.close();
			this.indexWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public IndexWriter getIndexWriter() {
		return this.indexWriter;
	}

	public Directory getIndexDir() {
		return indexDir;
	}

	public File getIndexDirPath() {
		return indexDirPath;
	}

	public boolean delete(Book book) {
		Term delTerm = new Term("id", "" + book.getId());
		try {
			synchronized (this) {
				this.indexWriter.deleteDocuments(delTerm);
				this.indexWriter.commit();
				this.indexWriter.deleteUnusedFiles();
				this.indexWriter.forceMergeDeletes();
				this.indexWriter.commit();
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public boolean add(Document doc) {
		try {
			synchronized (this) {
				this.indexWriter.addDocument(doc);
				this.indexWriter.commit();
			}
			return true;
		} catch (IOException e) {
			return false;
		}

	}

	public boolean updateDocument(Book book) {
		try {
			DirectoryReader reader = DirectoryReader.open(this.indexDir);
			IndexSearcher is = new IndexSearcher(reader);
			Query query = new TermQuery(new Term("id", "" + book.getId()));
			TopScoreDocCollector collector = TopScoreDocCollector.create(10,
					true);
			is.search(query, collector);

			ScoreDoc[] scoreDocs = collector.topDocs().scoreDocs;
			if (scoreDocs.length > 0) {
				int docID = scoreDocs[0].doc;
				Document doc = is.doc(docID);
				if (doc != null) {
					List<IndexableField> fields = book.getIndexableFields();
					for (IndexableField field : fields) {
						doc.removeFields(field.name());
					}
					for (IndexableField field : fields) {
						doc.add(field);
					}
					try {
						synchronized (this) {
							this.indexWriter.updateDocument(new Term("id", ""
									+ book.getId()), doc);
							this.indexWriter.commit();
							this.indexWriter.deleteUnusedFiles();
							this.indexWriter.forceMergeDeletes();
							this.indexWriter.commit();
							return true;
						}
					} catch (IOException e) {
					}
				}
			}

			return false;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean index(PDFBook book) {
		DocumentHandler handler = new PDFHandler();
		try {
			this.indexWriter.addDocument(handler.getDocument(book,
					bookService.findOne(book.getId())));
			this.indexWriter.commit();
			spellcheckerIndex();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	private void spellcheckerIndex() throws IOException {
		this.indexReader = DirectoryReader.open(indexDir);
		this.spellChecker = new SpellChecker(spellcheckerIndexDir);
		IndexWriterConfig iwc = new IndexWriterConfig(v, new SerbianAnalyzer(v));
		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		this.spellChecker.indexDictionary(new LuceneDictionary(indexReader, "text"), iwc, true);
		this.spellChecker.close();
	}

	protected void finalize() throws Throwable {
		this.indexWriter.close();
	}

}