package ftn.udd.projekt.integration.event;

import ftn.udd.projekt.integration.model.BookIntegration;
import ftn.udd.projekt.integration.model.UserIntegration;

public class BookViewedEvent2 {
	private UserIntegration user;
	private BookIntegration book;

	public BookViewedEvent2(UserIntegration user, BookIntegration book) {
		this.user = user;
		this.book = book;
	}

	public BookViewedEvent2() {
	}

	public UserIntegration getUser() {
		return user;
	}

	public void setUser(UserIntegration user) {
		this.user = user;
	}

	public BookIntegration getBook() {
		return book;
	}

	public void setBook(BookIntegration book) {
		this.book = book;
	}

}
