package ftn.udd.projekt.service.impl.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.repository.AuthorRepository;
import ftn.udd.projekt.service.def.AuthorService;

@Service
public class DBAuthorService implements AuthorService {

	@Autowired
	private AuthorRepository authorRepository;
	
	@Override
	public Author findOne(Integer id) {
		return authorRepository.findOne(id);
	}

	@Override
	public List<Author> findAll() {
		return authorRepository.findAll();
	}

	@Override
	public Author save(Author item) {
		return authorRepository.save(item);
	}

	@Override
	public void delete(Integer id) {
		authorRepository.delete(id);;
	}

	@Override
	public Author findOneByName(String name) {
		return authorRepository.findByName(name);
	}

}
