/* HEADER */

UDD.HeaderView = UDD.View
		.extend({
			el : "#header",

			template : Handlebars.compile($("#header-template").html()),

			render : function() {
				$(this.el).html(this.template());

				if (UDD.user) {
					$(".loggedInCheck").addClass("hidden");

					if (UDD.user.attributes && UDD.user.attributes.id) {
						$(".loggedInItem").removeClass("hidden");

						if (UDD.user.attributes.admin) {
							$(".adminItem").removeClass("hidden");
						}
					} else {
						$(".loggedOutItem").removeClass("hidden");
					}

				}

				return this;
			},

			events : {
				"click #search-button" : "search",
				"keypress #search-input" : "search",
				"click #signinAnchor" : "showSignIn",
				"click #registerAnchor" : "showRegister",
				"click #userAnchor" : "user"
			},

			search : function(ev) {
				if (ev.which == 1 || ev.which == 13) {
					var query = {};
					var title = $("#search-input").val();

					query.type = "REGULAR";
					query.title = query.authors = query.text = title;
					query.titleOperator = query.authorsOperator = query.textOperator = "SHOULD";
					query.suggestDidYouMean = true;
					query = Base64.encode(JSON.stringify(query));
					
					UDD.appRouter.navigate("search/" + query, true);
				}
			},

			showSignIn : function(ev) {
				var modal = new UDD.SignInModalView();
				UDD.ViewManager.showView(modal);
				$("#signinModal").modal();
			},

			showRegister : function(ev) {
				var modal = new UDD.RegisterModalView();
				UDD.ViewManager.showView(modal);
				$("#registerModal").modal();
			},

			user : function() {
				UDD.appRouter.navigate("user", true);
			}
		});

/* LEFT MENU */

UDD.LeftMenuView = UDD.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},

	el : "#left-menu",

	template : Handlebars.compile($("#left-menu-template").html()),

	render : function() {
		$(this.el).html(this.template({
			items : this.options.items.toJSON(),
			selectedId : this.options.selected
		}));
		return this;
	}
});

/* USER VIEW */

UDD.UserView = UDD.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},

	el : "#content",

	events : {
		"click #editProfileButton" : "editProfile",
		"click #saveProfileButton" : "saveProfile"
	},

	template : Handlebars.compile($("#user-template").html()),

	render : function() {
		$(this.el).html(this.template({
			categories : this.options.categories.toJSON(),
			user : this.options.user
		}));

		return this;
	},

	editProfile : function() {
		$(".form-control[disabled]").removeAttr("disabled");
		$("#inputName").focus();
		$("#editProfileButton").addClass("hidden");
		$("#saveProfileButton").removeClass("hidden");
	},

	saveProfile : function() {
		var users = this.collection;
		var id = $("#inputId").val();
		var name = $("#inputName").val();
		var lastname = $("#inputLastName").val();
		var email = $("#inputEmail").val();
		var password = $("#inputPassword").val();
		var subscribedTo = $("#inputSubscription").val();

		var newUser = new UDD.User({
			id : id,
			firstName : name,
			lastName : lastname,
			email : email,
			password : password,
			subscribedTo : subscribedTo,
			admin : false
		});

		if (newUser.isValid()) {
			newUser.save(null, {
				success : function(model, response) {
					$(".form-control").blur();
					$(".user-control").attr("disabled", "disabled");
					$("#editProfileButton").removeClass("hidden");
					$("#saveProfileButton").addClass("hidden");

					UDD.user = model;
				},
				error : function(model, response) {
					$("#inputErrorTextHolder").addClass("text-danger")
							.removeClass("text-success");
					$("#inputErrorTextHolder").html(
							"Error while updating profile. Reason: "
									+ response.statusText);
					console.log(response);
				}
			});
		} else {
			$("#inputErrorTextHolder").addClass("text-danger").removeClass(
					"text-success");
			$("#inputErrorTextHolder").text(newUser.validationError);
		}
	}
});

/* BOOKS IN CATEGORY */

UDD.CategoryBooksView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},
	el : "#content",

	template : Handlebars.compile($("#category-books-template").html()),

	render : function() {
		$(this.el).html(this.template({
			items : this.options.items.toJSON(),
			categoryTitle : this.options.title
		}));
		return this;
	},

	events : {
		"click .panel-heading" : "bookItemClicked",
		"click .book-title" : "bookItemClicked",
	},

	bookItemClicked : function(event) {
		var elId = $(event.currentTarget).parent().parent();
		var id = elId.attr("id");

		if (!id) {
			id = elId.parent().attr("id");
		}

		UDD.appRouter.navigate("book/" + id, true);
	}
});

/* SUGGESTED BOOKS VIEW */

UDD.SuggestedBooksView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},
	el : "#suggested-books",

	template : Handlebars.compile($("#suggested-books-template").html()),

	render : function() {
		$(this.el).html(this.template({
			items : this.collection.toJSON(),
		}));
		return this;
	},

	events : {
		"click .book-image-suggestion" : "bookItemClicked",
		"click .book-link" : "bookItemClicked"
	},

	bookItemClicked : function(event) {
		var elId = $(event.currentTarget).parent();
		var id = elId.attr("id");

		if (!id) {
			id = elId.parent().attr("id");
		}

		UDD.appRouter.navigate("book/" + id, true);
	}
});

/* BOOK INFO VIEW */

UDD.BookInfoView = Backbone.View
		.extend({
			initialize : function(options) {
				this.options = options || {};
			},

			el : "#left-menu",

			events : {
				"click #downloadButton" : "downloadBook"
			},

			template : Handlebars.compile($("#book-info-template").html()),

			render : function() {
				$(this.el).html(this.template({
					item : this.options.item
				}));
				return this;
			},

			downloadBook : function() {
				var user = UDD.user.attributes;
				if (user.admin
						|| (user.subscribedTo && (user.subscribedTo.id == 0 || user.subscribedTo.id == this.options.item.category.id))) {
					window.location = appConfig.downloadPath
							+ this.options.item.id;
				} else {
					var modal = new UDD.SubscribeModalView({
						category : this.options.item.category
					});
					UDD.ViewManager.showView(modal);
					$("#subscribeModal").modal();
				}
			}
		});

/* BOOK CONTENT VIEW */

UDD.BookContentView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},

	el : "#content",

	template : Handlebars.compile($("#book-content-template").html()),

	render : function() {
		$(this.el).html(this.template({
			item : this.options.item
		}));
		var books = new UDD.SuggestedBooks({
			id : this.options.item.id
		});
		books.fetch({
			success : function(model, response, options) {
				var view = new UDD.SuggestedBooksView({
					collection : model
				});
				UDD.ViewManager.showView(view);
			},
			error : function(model, response, options) {
				console.log("Error fetching book suggestions. Response: ");
				console.log(response);
			}
		});
		return this;
	}
});

/* SEARCH RESULTS VIEW */

UDD.SearchResultsView = UDD.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},

	el : "#content",
	
	events : {
		"click #didYouMeanAnchor" : "didYouMean",
		"click .book-description" : "viewBook",
		"click .title-string" : "viewBook"
	},

	template : Handlebars.compile($("#search-results-template").html()),

	render : function() {
		$(this.el).html(this.template({
			items : this.options.items.toJSON(),
			didYouMean : this.options.didYouMean
		}));
		return this;
	},
	
	didYouMean : function(ev) {
		ev.stopImmediatePropagation();
		ev.preventDefault();
		var query = {};
		var title = this.options.didYouMean;

		query.type = "REGULAR";
		query.title = query.authors = query.text = title;
		query.titleOperator = query.authorsOperator = query.textOperator = "SHOULD";
		query.suggestDidYouMean = true;
		query = Base64.encode(JSON.stringify(query));

		UDD.appRouter.navigate("search/" + query, true);
	},
	
	viewBook : function(ev) {
		var id = $(ev.currentTarget).closest(".book-item").attr("id");
		UDD.appRouter.navigate("book/" + id, true)
	}
});

/* ADVANCED SEARCH VIEW */

UDD.AdvancedSearchView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},

	el : "#content",

	template : Handlebars.compile($("#advanced-search-template").html()),

	render : function() {
		$(this.el).html(this.template());
		return this;
	},

	events : {
		"click #advanced-search-button" : "advancedSearch"
	},

	advancedSearch : function() {
		var query = {};
		query.type = $("#searchType").val();
		query.title = $("#searchTitle").val();
		query.titleOperator = $("#titleOperator").val();
		query.authors = $("#searchAuthors").val();
		query.authorsOperator = $("#authorsOperator").val();
		query.keywords = $("#searchKeywords").val();
		query.keywordsOperator = $("#keywordsOperator").val();
		query.text = $("#searchText").val();
		query.textOperator = $("#textOperator").val();
		query.language = $("#searchLanguage").val();
		query.languageOperator = $("#languageOperator").val();
		query.category = $("#searchCategory").val();
		query.categoryOperator = $("#categoryOperator").val();

		query = Base64.encode(JSON.stringify(query));
		UDD.appRouter.navigate("search/" + query, true);
	}
});

/* SIGN IN MODAL */

UDD.SignInModalView = UDD.View
		.extend({
			el : "#modal",

			initialize : function(options) {
				this.options = options || {};
			},

			template : Handlebars.compile($("#signin-modal-template").html()),

			events : {
				"click #signinButton" : "signIn"
			},

			render : function() {
				var self = this;
				$(this.el).html(this.template());
				$('#signinModal').on('hidden.bs.modal', function(e) {
					self.unset();
				});
				UDD.modal = this;
				return this;
			},

			signIn : function(ev) {
				var username = $("#signinEmail").val();
				var password = $("#signinPassword").val();
				$
						.ajax({
							url : Spring.securityUrl,
							type : 'POST',
							data : {
								'j_username' : username,
								'j_password' : password
							},
							dataType : 'json',
							success : function(data) {
								if (data.id) {
									$(".errorblock").addClass("hidden");
									UDD.user = new UDD.User(data);

									location.reload();
								} else {
									$(".errorblock")
											.text(
													Spring.messages["error.text.authenticationFailed"]);
									$(".errorblock").removeClass("hidden");
								}
							},
							error : function(resp) {
								$(".errorblock")
										.text(
												Spring.messages[resp.responseJSON.message]);
								$(".errorblock").removeClass("hidden");
							}
						});
			}
		});

/* SUBSCRIBE MODAL */

UDD.SubscribeModalView = UDD.View.extend({
	el : "#modal",

	initialize : function(options) {
		this.options = options || {};
	},

	template : Handlebars.compile($("#subscribe-modal-template").html()),

	events : {
		"click #categorySubButton" : "subscribeOne",
		"click #allSubButton" : "subscribeAll"
	},

	render : function() {
		var self = this;
		$(this.el).html(this.template({
			category : this.options.category
		}));
		$('#subscribeModal').on('hidden.bs.modal', function(e) {
			self.unset();
		});
		UDD.modal = this;
		return this;
	},

	subscribeOne : function() {
		this.subscribe(this.options.category.id)
	},

	subscribeAll : function() {
		this.subscribe(0);
	},

	subscribe : function(id) {
		$.ajax({
			url : appConfig.appRootPath + "/users/subscribe/" + id,
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				console.log(data);
				UDD.user.set("subscribedTo", data);
				$('#subscribeModal').modal("hide");
			},
			error : function(resp) {
				$('#subscribeModal').modal("hide");
			}
		});
	}
});

/* REGISTER MODAL */

UDD.RegisterModalView = UDD.View.extend({
	el : "#modal",

	initialize : function(options) {
		this.options = options || {};
	},

	template : Handlebars.compile($("#register-modal-template").html()),

	render : function() {
		var self = this;
		$(this.el).html(this.template());
		$('#registerModal').on('hidden.bs.modal', function(e) {
			self.unset();
		});
		UDD.modal = this;
		return this;
	},

	events : {
		"click #register-button" : "register"
	},

	register : function() {
		var users = this.collection;
		var name = $('#inputName').val();
		var lastname = $('#inputLastName').val();
		var email = $('#inputEmail').val();
		var password = $('#inputPassword').val();

		var newUser = new UDD.User({
			firstName : name,
			lastName : lastname,
			email : email,
			password : password
		});

		if (newUser.isValid()) {
			newUser.save(null, {
				success : function(model, response) {
					location.reload();
				},
				error : function(model, response) {
					$("#input-error-text-holder").addClass("text-danger")
							.removeClass("text-success");
					$("#input-error-text-holder").text(
							"Error while registering. Response: "
									+ response.toJSON());
				}
			});
		} else {
			$(".errorblock").text(newUser.validationError);
			$(".errorblock").removeClass("hidden");
		}
	}
});

/* INDEX */

UDD.IndexView = Backbone.View.extend({
	el : "#content",

	template : Handlebars.compile($("#index-template").html()),

	render : function() {
		$(this.el).html(this.template());
		return this;
	}
});