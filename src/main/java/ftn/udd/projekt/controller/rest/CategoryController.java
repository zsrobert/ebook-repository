package ftn.udd.projekt.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.udd.projekt.dto.CategoryDTO;
import ftn.udd.projekt.dto.FieldDTO;
import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.service.def.CategoryService;
import ftn.udd.projekt.util.ClassFieldUtils;

@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/rest/categories", method = RequestMethod.GET)
	public List<CategoryDTO> handleGetAllCategories() {
		ArrayList<CategoryDTO> list = new ArrayList<>();
		
		for (Category cat : categoryService.findAll()) {
			list.add(new CategoryDTO(cat));
		}
		
		return list;
	}

	@RequestMapping(value = "/rest/categories/{id}", method = RequestMethod.GET)
	public ResponseEntity<CategoryDTO> handleGetOneCategory(
			@PathVariable Integer id) {
		Category category = categoryService.findOne(id);
		ResponseEntity<CategoryDTO> response;

		if (category != null) {
			response = new ResponseEntity<CategoryDTO>(new CategoryDTO(category), HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return response;
	}

	@RequestMapping(value = "/rest/categories", method = RequestMethod.POST)
	public ResponseEntity<CategoryDTO> handlePostCategory(@Valid @RequestBody CategoryDTO category) {
		Category cat = categoryService.save(new Category(category));
		ResponseEntity<CategoryDTO> response;
		if (cat != null && cat.getId() != null) {
			response = new ResponseEntity<CategoryDTO>(new CategoryDTO(cat), HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/rest/categories/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<CategoryDTO> handleDeleteCategory(@PathVariable Integer id) {
		Category category = categoryService.findOne(id);
		ResponseEntity<CategoryDTO> response;
		
		if (category != null) {
			categoryService.delete(id);
			response = new ResponseEntity<>(new CategoryDTO(category), HttpStatus.OK);
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/rest/categories/fields", method = RequestMethod.GET)
	public List<FieldDTO> handleGetFields() {
		return ClassFieldUtils.getFieldDTOs(CategoryDTO.class);
	}

}
