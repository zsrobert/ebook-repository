package ftn.udd.projekt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ftn.udd.projekt.model.Book;

public interface BookRepository extends JpaRepository<Book, Integer> {

	@Query("SELECT b FROM Book b WHERE category.id = :id")
	public List<Book> findAllInCategory(@Param("id") Integer id);
	
	@Query("SELECT b FROM Book b, IN (b.authors) AS a WHERE a.id = :id")
	public List<Book> findAllByAuthor(@Param("id") Integer id);
}
