package ftn.udd.projekt.integration.event;

import org.springframework.context.ApplicationEvent;

import ftn.udd.projekt.integration.model.BookIntegration;
import ftn.udd.projekt.integration.model.UserIntegration;

public class BookViewedEvent extends ApplicationEvent {
	private static final long serialVersionUID = -2873980380251928055L;
	private UserIntegration user;
	private BookIntegration book;

	public BookViewedEvent(Object source, UserIntegration user, BookIntegration book) {
		super(source);
		this.user = user;
		this.book = book;
	}

	public BookViewedEvent(Object source) {
		super(source);
	}

	public UserIntegration getUser() {
		return user;
	}

	public void setUser(UserIntegration user) {
		this.user = user;
	}

	public BookIntegration getBook() {
		return book;
	}

	public void setBook(BookIntegration book) {
		this.book = book;
	}

}
