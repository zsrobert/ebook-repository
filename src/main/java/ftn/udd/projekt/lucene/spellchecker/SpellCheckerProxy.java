package ftn.udd.projekt.lucene.spellchecker;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SpellCheckerProxy {

	@Value("${location.spellchecker.index}")
	private String spellcheckerIndexPath;

	private Directory spellcheckerIndexDir;

	private SpellChecker spellChecker;

	public SpellCheckerProxy() {

	}

	@PostConstruct
	private void init() {
		File scFile = new File(spellcheckerIndexPath);
		if (!scFile.exists()) {
			scFile.mkdirs();
		}

		try {
			this.spellcheckerIndexDir = new SimpleFSDirectory(scFile);
			this.spellChecker = new SpellChecker(spellcheckerIndexDir);
		} catch (IOException ioe) {
			throw new IllegalArgumentException("Path not correct");
		}
	}

	public String spellcheck(String input) {
		try {
			return spellChecker.suggestSimilar(input, 10, 0.7f)[0];
		} catch (Exception e) {
			return null;
		}
	}
}
