package ftn.udd.projekt.dto;

import java.util.List;

import ftn.udd.projekt.model.SearchResultBook;

public class SearchResultsWrapper {
	private String didYouMean;
	private List<SearchResultBook> data;

	public String getDidYouMean() {
		return didYouMean;
	}

	public void setDidYouMean(String didYouMean) {
		this.didYouMean = didYouMean;
	}

	public List<SearchResultBook> getData() {
		return data;
	}

	public void setData(List<SearchResultBook> data) {
		this.data = data;
	}

}
