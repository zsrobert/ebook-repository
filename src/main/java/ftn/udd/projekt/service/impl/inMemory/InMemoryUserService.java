package ftn.udd.projekt.service.impl.inMemory;

import java.util.ArrayList;
import java.util.List;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.model.User;
import ftn.udd.projekt.service.def.UserService;

public final class InMemoryUserService extends InMemoryBaseService<User>
		implements UserService {

	@Override
	public User findByEmail(String email) {
		for (User user : items.values()) {
			if (user.getEmail().equals(email)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public List<User> findBySubscription(Category category) {
		List<User> users = new ArrayList<User>();

		for (User user : items.values()) {
			if (user.getSubscription() != null
					&& (user.getSubscription().getCategory() == null || user
							.getSubscription().getCategory().equals(category))) {
				users.add(user);
			}
		}

		return users;
	}
}
