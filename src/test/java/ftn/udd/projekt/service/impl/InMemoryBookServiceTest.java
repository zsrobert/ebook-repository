package ftn.udd.projekt.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ftn.udd.projekt.model.Author;
import ftn.udd.projekt.model.Book;
import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.service.def.BookService;
import ftn.udd.projekt.service.impl.inMemory.InMemoryBookService;

public class InMemoryBookServiceTest {

	private BookService bookService;
	
	@Before
	public void setUp() throws Exception {
		this.bookService = new InMemoryBookService();
		
		Book cuprija = new Book();
		cuprija.setTitle("Na Drini cuprija");
		Author ivo = new Author();
		ivo.setName("Ivo");
		cuprija.addAuthor(ivo);
		Category category = new Category();
		category.setName("Roman");
		cuprija.setCategory(category);
		cuprija.setDescription("Bla bla");
		List<String> keywords = new ArrayList<String>();
		keywords.add("bla");
		keywords.add("Meh");
		keywords.add("blah");
		cuprija.setKeyWords(keywords);
		cuprija.setLanguage("Srpski");
		cuprija.setPublishingDate(new Date());
		
		Book kraljoubica = new Book();
		kraljoubica.setTitle("Legenda o kraljoubici");
		
		bookService.save(cuprija);
		bookService.save(kraljoubica);
	}

	@Test
	public void testfindOne() {
		Book book = bookService.findOne(1);
		
		Assert.assertNotNull(book);
		Assert.assertEquals("Na Drini cuprija", book.getTitle());
		Assert.assertEquals(1, book.getAuthors().size());
		Assert.assertEquals("Roman", book.getCategory().getName());
		Assert.assertEquals("Bla bla", book.getDescription());
		Assert.assertEquals("Srpski", book.getLanguage());
		Assert.assertEquals("bla", book.getKeyWords().get(0));
		Assert.assertNotNull(book.getPublishingDate());
	}

	@Test
	public void testFindAll() {
		List<Book> books = bookService.findAll();
		
		Assert.assertEquals(2, books.size());
		
		Book book1 = books.get(0);
		Book book2 = books.get(1);
		
		if (book1.getId().equals(1)) {
			Assert.assertEquals("Na Drini cuprija", book1.getTitle());
			
			Assert.assertTrue(book2.getId().equals(2) && "Legenda o kraljoubici".equals(book2.getTitle()));
		} else {
			Assert.assertEquals("Legenda o kraljoubici", book1.getTitle());
			
			Assert.assertTrue(book2.getId().equals(1) && "Na Drini cuprija".equals(book2.getTitle()));
		}
	}

	@Test
	public void testSave() {
		Book book = new Book();
		book.setTitle("Nova");
		
		book = bookService.save(book);
		
		Assert.assertNotNull(book);
		Assert.assertNotNull(book.getId());
		
		Assert.assertEquals("Nova", book.getTitle());
	}

	@Test
	public void testDelete() {
		Assert.assertNotNull(bookService.findOne(1));
		Assert.assertNotNull(bookService.findOne(2));
		
		bookService.delete(1);
		
		Assert.assertNull(bookService.findOne(1));
		Assert.assertNotNull(bookService.findOne(2));
	}

}
