package ftn.udd.projekt.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SearchResultBook extends BaseModel {
	private String title;
	private List<Author> authors;
	private String description;
	private List<String> keyWords;
	private String language;
	private Date publishingDate;
	private Category category;
	private String highlightedText;
	
	public SearchResultBook() {
		super();
	}
	
	public SearchResultBook(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.description = book.getDescription();
		this.language = book.getLanguage();
		this.category = book.getCategory();
		this.publishingDate = book.getPublishingDate();
		this.authors = new ArrayList<Author>(book.getAuthors());
		this.keyWords = new ArrayList<String>(book.getKeyWords());
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(List<String> keyWords) {
		this.keyWords = keyWords;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Date getPublishingDate() {
		return publishingDate;
	}

	public void setPublishingDate(Date publishingDate) {
		this.publishingDate = publishingDate;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getHighlightedText() {
		return highlightedText;
	}

	public void setHighlightedText(String highlightedText) {
		this.highlightedText = highlightedText;
	}

}
