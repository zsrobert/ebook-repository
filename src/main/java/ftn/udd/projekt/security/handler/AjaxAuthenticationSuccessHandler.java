package ftn.udd.projekt.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.ELRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import ftn.udd.projekt.dto.UserDTO;
import ftn.udd.projekt.model.User;
import ftn.udd.projekt.service.def.UserService;

@Component(value = "ajaxAuthenticationSuccessHandler")
public class AjaxAuthenticationSuccessHandler extends
		SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private UserService userService;

	private static final RequestMatcher REQUEST_MATCHER = new ELRequestMatcher(
			"hasHeader('X-Requested-With','XMLHttpRequest')");

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		if (isAjaxRequest(request)) {
			response.setContentType("application/json;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			UserDetails userDetails = (UserDetails) authentication
					.getPrincipal();
			User user = userService.findByEmail(userDetails.getUsername());

			if (user != null) {
				ObjectMapper mapper = new ObjectMapper();
				String json = mapper.writeValueAsString(new UserDTO(user));
				response.getWriter().write(json);
			} else {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
			}

		} else {
			super.onAuthenticationSuccess(request, response, authentication);
		}
	}

	private boolean isAjaxRequest(HttpServletRequest request) {
		return REQUEST_MATCHER.matches(request);
	}

}
