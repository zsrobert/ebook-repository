package ftn.udd.projekt.dto;

public class BaseDTO {
	protected Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}