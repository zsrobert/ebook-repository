/* HEADER */

UDD.HeaderView = Backbone.View.extend({
	el : "#header",

	template : Handlebars.compile($("#header-template").html()),

	render : function() {
		$(this.el).html(this.template());
		return this;
	}
});
/* LEFT MENU */

UDD.LeftMenuView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
	},
	el : "#left-menu",

	template : Handlebars.compile($("#left-menu-template").html()),

	render : function() {
		$(this.el).html(this.template({
			activeItem : this.options.activeItem
		}));
		return this;
	}
});

/* CONTENT */

UDD.ContentView = Backbone.View
		.extend({
			initialize : function(options) {
				this.options = options || {};
			},

			el : "#content",

			template : Handlebars.compile($("#content-template").html()),

			render : function() {
				$(this.el).html(this.template({
					title : this.options.title
				}));

				var superOptions = this.options;

				if (this.options.controllerView) {
					var controllerView = new this.options.controllerView({
						collection : this.options.bodyCollection
					});
					controllerView.render();
				}

				this.options.headerCollection
						.fetch({
							success : function(model, response) {
								var contentTableHeaderView = new UDD.ContentTableHeaderView(
										{
											fields : model,
											editable : superOptions.editable,
											imageUploadable : superOptions.imageUploadable
										});
								contentTableHeaderView.render();
							},
							error : function(model, response) {
								console
										.log("Error fetching categories. Response: "
												+ response);
							}
						});

				this.options.bodyCollection
						.fetch({
							success : function(model, response) {
								var contentTableBodyView = new UDD.ContentTableBodyView(
										{
											items : model,
											editable : superOptions.editable,
											imageUploadable : superOptions.imageUploadable
										});
								contentTableBodyView.render();
							},
							error : function(model, response) {
								console
										.log("Error fetching categories. Response: "
												+ response);
							}
						});

				return this;
			}
		});

/* CONTENT TABLE HEADER */

UDD.ContentTableHeaderView = Backbone.View.extend({
	el : "#content-table-header",

	initialize : function(options) {
		this.options = options || {};
	},

	template : Handlebars.compile($("#content-table-header-template").html()),

	render : function() {
		$(this.el).html(this.template({
			fields : this.options.fields.toJSON(),
			editable : this.options.editable,
			imageUploadable : this.options.imageUploadable
		}));
		return this;
	}
});

/* CONTENT TABLE BODY */

UDD.ContentTableBodyView = Backbone.View.extend({
	el : "#content-table-body",

	initialize : function(options) {
		this.options = options || {};
		this.options.items.bind("reset", this.render, this);
		this.options.items.bind("remove", this.render, this);
		this.options.items.bind("add", this.render, this);
		this.options.items.bind("change", this.render, this);
	},

	events : {
		"click .btn-rst" : "deleteItem",
		"click .btn-img" : "uploadImage",
		"click .btn-edt" : "editItem"
	},

	template : Handlebars.compile($("#content-table-body-template").html()),

	render : function() {
		$(this.el).html(this.template({
			items : this.options.items.toJSON(),
			editable : this.options.editable,
			imageUploadable : this.options.imageUploadable
		}));
		if (this.options.imageUploadable) {
			this.postRender();
		}
		return this;
	},

	deleteItem : function(ev) {
		var response = confirm("Are you sure?");
		if (response) {
			var id = $(ev.currentTarget).attr("id").slice(4);
			var model = this.options.items.get(id);
			var self = this;
			model.destroy({
				success : function() {
					self.options.items.remove(model);
				},
				error : function() {
					alert("Couldn't delete item!");
				}
			});

		}
	},

	editItem : function(ev) {
		var id = $(ev.currentTarget).attr("id").slice(4);
		var self = this;
		var book = this.options.items.get(id);
		console.log(book.toJSON());
		// render modal
		UDD.getCategories(function(categories) {
			var modalView = new UDD.AddBookModalView({
				item : book.toJSON(),
				categories : categories.toJSON(),
				books : self.options.items,
				message : "Book edited successfully!",
				editing : true,
				cancelAddBook : function() {
					var self = this;
					$('#add-book-modal').on('hidden.bs.modal', function(e) {
						self.unset();
					});
				}
			});
			modalView.render();

			// show modal
			$('#add-book-modal').modal('show');
		});
	},

	postRender : function() {
		var url = window.location.hostname === "localhost" ? "books/upload"
				: "books/upload";
		var books = this.collection;
		$(".btn-img").fileupload({
			url : "",
			maxNumberOfFiles : 1,
			add : function(e, data) {
				data.url = "books/" + e.target.id.slice(4) + "/image/upload";
				$(e.target).removeClass("glyphicon glyphicon-picture");
				$(e.target).addClass("fa fa-spinner fa-spin");
				data.submit();
			},
			done : function(e, data) {
				setTimeout(function() {
					$(e.target).removeClass("fa fa-spinner fa-spin");
					$(e.target).addClass("glyphicon glyphicon-picture");
				}, 1000);
			}
		}).prop("disabled", !$.support.fileInput).parent().addClass(
				$.support.fileInput ? undefined : "disabled");
	}
});

/* ADD CATEGORY CONTROL */

UDD.AddCategoryView = Backbone.View
		.extend({
			el : '#content-control',

			initialize : function(options) {
				this.options = options || {};
			},

			template : Handlebars.compile($(
					"#category-content-control-template").html()),

			render : function() {
				$(this.el).html(this.template());
				return this;
			},

			events : {
				"click #add-category-button" : "submitNewCategory",
				'keypress input[name="category-name"]' : "submitNewCategory"
			},

			submitNewCategory : function(ev) {
				if (ev.which == 1 || ev.which == 13) {
					var categories = this.collection;
					var categoryElement = $('input[name="category-name"]');
					var categoryName = categoryElement.val();
					var newCategory = new UDD.Category({
						name : categoryName
					});

					if (newCategory.isValid()) {
						categoryElement.parent().removeClass("has-error");
						newCategory
								.save(
										null,
										{
											success : function(model, response) {
												categories.add(model);
												$("#input-error-text-holder")
														.removeClass(
																"text-danger")
														.addClass(
																"text-success");
												$("#input-error-text-holder")
														.text(
																"Category added successfully.");
											},
											error : function(model, response) {
												$("#input-error-text-holder")
														.addClass("text-danger")
														.removeClass(
																"text-success");
												$("#input-error-text-holder")
														.text(
																"Error while saving new category. Response: "
																		+ response
																				.toJSON());
											}
										});
					} else {
						categoryElement.parent().addClass("has-error");
						$("#input-error-text-holder").addClass("text-danger")
								.removeClass("text-success");
						$("#input-error-text-holder").text(
								newCategory.validationError);
					}
				}
			}
		});

/* ADD BOOK CONTROL */

UDD.AddBookView = Backbone.View.extend({
	el : '#content-control',

	initialize : function(options) {
		this.options = options || {};
	},

	template : Handlebars.compile($("#book-content-control-template").html()),

	render : function() {
		$(this.el).html(this.template());
		this.postRender();
		return this;
	},

	postRender : function() {
		var url = window.location.hostname === "localhost" ? "books/upload"
				: "books/upload";
		var books = this.collection;
		$("#fileupload").fileupload(
				{
					url : url,
					dataType : "json",
					maxNumberOfFiles : 1,
					add : function(e, data) {
						$("#upload-progress-bar").parent().css("visibility",
								"visible");
						data.submit();
					},
					done : function(e, data) {
						console.log(data);
						setTimeout(function() {
							$("#upload-progress-bar").parent().css(
									"visibility", "hidden");
							$("#upload-progress-bar").css("width", "0%");
						}, 1000);

						// render modal
						UDD.getCategories(function(categories) {
							var modalView = new UDD.AddBookModalView({
								item : data.result,
								categories : categories.toJSON(),
								books : books
							});
							modalView.render();

							// show modal
							$('#add-book-modal').modal('show');
						});
					},
					progressall : function(e, data) {
						var progress = parseInt(data.loaded / data.total * 100,
								10);
						$("#upload-progress-bar").css("width", progress + "%");
					}
				}).prop("disabled", !$.support.fileInput).parent().addClass(
				$.support.fileInput ? undefined : "disabled");
	}
});

/* ADD BOOK MODAL */

UDD.AddBookModalView = UDD.View
		.extend({
			el : "#modal",

			initialize : function(options) {
				this.options = options || {};

				if (this.options.cancelAddBook) {
					this.cancelAddBook = this.options.cancelAddBook;
				}

				if (!this.options.message) {
					this.options.message = "Book added successfully! It will be visible once it's been indexed.";
				}
			},

			template : Handlebars.compile($("#add-book-modal-template").html()),

			render : function() {
				$(this.el).html(this.template({
					item : this.options.item,
					categories : this.options.categories
				}));
				return this;
			},

			events : {
				"click #cancel-button" : "cancelAddBook",
				"click #save-button" : "saveBook",
				"click .close" : "cancelAddBook"
			},

			cancelAddBook : function() {
				var book = new UDD.Book({
					id : this.options.item.id
				});
				book.destroy();
				var self = this;
				$('#add-book-modal').on('hidden.bs.modal', function(e) {
					self.unset();
				});
			},

			saveBook : function() {
				var book = {};
				book.id = this.options.item.id;
				book.title = $("#bookTitle").val();
				book.description = $("#bookDescription").val();
				book.title = $("#bookTitle").val();
				book.authors = $("#bookAuthors").val();
				book.category = {};
				book.category.name = "mnjeh";
				book.category.id = $("#bookCategory").val();
				book.publishingDate = $("#bookDate").val();
				book.language = $("#bookLanguage").val();
				book.keyWords = $("#bookKeywords").val();
				var bookModel = new UDD.Book(book);
				var books = this.options.books;
				var self = this;

				if (bookModel.isValid()) {
					$(".errorblock").addClass("hidden");
					bookModel.save(null, {
						success : function(model) {
							if (!self.options.editing) {
								books.add(model);
							} else {
								books.set(model, {
									remove : false
								});
							}

							$("#add-book-modal").modal("hide");
							$(".alert").removeClass("alert-danger").addClass(
									"alert-success");
							$("#alert-text").text(self.options.message);
							$(".alert").show();
							$('#add-book-modal').on('hidden.bs.modal',
									function(e) {
										self.unset();
									});
						},
						error : function() {
							$("#add-book-modal").modal("hide");
							$(".alert").addClass("alert-danger").removeClass(
									"alert-success");
							$("#alert-text").text("Error adding the book!");
							$(".alert").show();
							$('#add-book-modal').on('hidden.bs.modal',
									function(e) {
										self.unset();
									});
						}
					});
				} else {
					$(".errorblock").text(bookModel.validationError);
					$(".errorblock").removeClass("hidden");
				}

			}
		});