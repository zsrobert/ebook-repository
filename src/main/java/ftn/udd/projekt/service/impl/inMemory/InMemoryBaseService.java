package ftn.udd.projekt.service.impl.inMemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import ftn.udd.projekt.model.BaseModel;
import ftn.udd.projekt.service.def.BaseService;

public class InMemoryBaseService<E extends BaseModel> implements BaseService<E> {

	protected Map<Integer, E> items = new HashMap<>();
	private final AtomicInteger sequence = new AtomicInteger(1);

	@Override
	public E findOne(Integer id) {
		return items.get(id);
	}

	@Override
	public List<E> findAll() {
		return new ArrayList<E>(items.values());
	}

	@Override
	public E save(E item) {
		if (item.getId() == null) {
			item.setId(sequence.getAndIncrement());
		}

		items.put(item.getId(), item);

		return item;
	}

	@Override
	public void delete(Integer id) {
		items.remove(id);
	}

}
