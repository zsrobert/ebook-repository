package ftn.udd.projekt.service.def;

import java.util.List;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.model.User;

public interface UserService extends BaseService<User> {
	public User findByEmail(String email);
	public List<User> findBySubscription(Category category);
}
