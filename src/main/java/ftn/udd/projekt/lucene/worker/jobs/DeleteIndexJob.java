package ftn.udd.projekt.lucene.worker.jobs;

import ftn.udd.projekt.lucene.indexer.Indexer;
import ftn.udd.projekt.model.Book;

public class DeleteIndexJob implements Runnable {
	private Book book;
	private Indexer indexer;
	
	public DeleteIndexJob(Book book, Indexer indexer) {
		super();
		this.book = book;
		this.indexer = indexer;
	}

	@Override
	public void run() {
		indexer.delete(book);
	}

}
