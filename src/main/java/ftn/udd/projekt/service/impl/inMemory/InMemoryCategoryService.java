package ftn.udd.projekt.service.impl.inMemory;

import ftn.udd.projekt.model.Category;
import ftn.udd.projekt.service.def.CategoryService;

public final class InMemoryCategoryService extends
		InMemoryBaseService<Category> implements CategoryService {

}
