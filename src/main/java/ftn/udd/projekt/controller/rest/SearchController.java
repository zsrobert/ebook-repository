package ftn.udd.projekt.controller.rest;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;

import org.apache.lucene.search.BooleanClause.Occur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ftn.udd.projekt.dto.BookStoreDTO;
import ftn.udd.projekt.dto.SearchDTO;
import ftn.udd.projekt.dto.SearchResultsWrapper;
import ftn.udd.projekt.lucene.model.SearchType;
import ftn.udd.projekt.lucene.searcher.ResultRetriever;
import ftn.udd.projekt.lucene.spellchecker.SpellCheckerProxy;

@RestController
public class SearchController {
	@Autowired
	private ResultRetriever resultRetriever;

	@Autowired
	private SpellCheckerProxy spellChecker;

	private ObjectMapper mapper = new ObjectMapper();

	@RequestMapping(value = "/rest/books/search", method = RequestMethod.GET)
	private SearchResultsWrapper handleSearch(
			@RequestParam(value = "query") String base64EncodedQuery) {
		SearchDTO searchDTO = null;
		SearchResultsWrapper results = new SearchResultsWrapper();

		try {
			String query = URLDecoder.decode(
					(new String(Base64.decode(base64EncodedQuery.getBytes()))),
					"utf-8");

			searchDTO = mapper.readValue(query, SearchDTO.class);

			if (searchDTO.isSuggestDidYouMean()) {
				results.setDidYouMean(spellChecker.spellcheck(searchDTO
						.getTitle()));
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		results.setData(resultRetriever.getResults(searchDTO));

		return results;
	}

	@RequestMapping(value = "/rest/books/similar/{id}", method = RequestMethod.GET)
	private List<BookStoreDTO> handleGetSimilar(@PathVariable Integer id) {
		return resultRetriever.getMoreLikeThis(id);
	}

	@RequestMapping(value = "/rest/books/test/get", method = RequestMethod.GET)
	private SearchDTO handleTestGET() {
		SearchDTO s = new SearchDTO();
		s.setAuthors("Prvi, Drugi");
		s.setAuthorsOperator(Occur.MUST);
		s.setCategory("Kategorija");
		s.setCategoryOperator(Occur.MUST_NOT);
		s.setKeywords("Prva, Druga");
		s.setKeywordsOperator(Occur.SHOULD);
		s.setLanguage("Jezik");
		s.setLanguageOperator(Occur.MUST);
		s.setText("Tekst");
		s.setTextOperator(Occur.MUST_NOT);
		s.setType(SearchType.PHRASE);
		return s;
	}
}
