package ftn.udd.projekt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.udd.projekt.model.Author;

public interface AuthorRepository extends JpaRepository<Author, Integer>{
	public Author findByName(String name);
}
