package ftn.udd.projekt.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ftn.udd.projekt.dto.FieldDTO;

public class ClassFieldUtils {
	public static List<FieldDTO> getFieldDTOs(Class<?> classObject) {
		ArrayList<FieldDTO> fieldDTOs = new ArrayList<FieldDTO>();

		Field[] fields = classObject.getDeclaredFields();

		for (int i = 0; i < fields.length; i++) {

			if (!"serialVersionUID".equals(fields[i].getName())) {
				if (!"id".equals(fields[i].getName())) {
					fieldDTOs.add(new FieldDTO("field."
							+ removeDTOSuffix(classObject.getSimpleName())
									.toLowerCase() + "."
							+ removeIsPrefix(fields[i].getName())));
				} else {
					fieldDTOs.add(new FieldDTO("field.any."
							+ removeIsPrefix(fields[i].getName())));
				}
			}
		}

		// ensure that the names are always sent in the same order

		Collections.sort(fieldDTOs);

		fieldDTOs.add(0, new FieldDTO("#"));

		return fieldDTOs;
	}

	private static String[] splitCamelCase(String input) {
		if (input != null) {
			return input.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
		} else {
			return null;
		}
	}

	private static String removeIsPrefix(String input) {
		if (input.startsWith("is")) {
			return input.replace("is", "");
		} else {
			return input;
		}
	}

	private static String removeDTOSuffix(String input) {
		if (input.endsWith("AdminDTO")) {
			return input.replace("AdminDTO", "");
		} else if (input.endsWith("DTO")) {
			return input.replace("DTO", "");
		} else {
			return input;
		}
	}

	public static String getSplitStringOnCamelCase(String input) {
		String[] tokens = splitCamelCase(input);
		String output = "";

		if (tokens != null) {
			output = StringUtils.capitalize(tokens[0]);
			for (int i = 1; i < tokens.length; i++) {
				output += " " + tokens[i].toLowerCase();
			}
		}

		return output;
	}
}
